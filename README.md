# Thimage - An Image Themer

Thimage takes images and extracts statistical information about them in order to
apply theme templates to create themes inspired by an image.

Internally Thimage currently is restricted to sRGB and the D65 white point.

## Image Analysis

The analysis performed on the images is described here. The goal of image analysis is to come up with the following information:

 - Brightness: Whether the image is bright or dark
 - Warmth: Whether the image is warm or cool
 - Light: The average of all of the lightest colours in the image
 - Dark: The average of all of the darkest colours in the image
 - Basic Palette: A colour palette for 3-bit colours
   - White
   - Black
   - Red
   - Green
   - Blue
   - Cyan
   - Magenta
   - Yellow
 - ANSI Palette: A colour palette for 4-bit colours
   - White
   - Black
   - Red
   - Green
   - Blue
   - Cyan
   - Magenta
   - Yellow
   - Alternate White
   - Alternate Black
   - Alternate Red
   - Alternate Green
   - Alternate Blue
   - Alternate Cyan
   - Alternate Magenta
   - Alternate Yellow
 - Theme Palette: A colour palette to best reflect the theme
   - Primary
   - Secondary
   - Tertiary
   - Quaternary
   - Quinary
   - Senary
   - ... (until no more useful colours are found)
   - Background (contrasts with primary)
     - If primary is light, this is dark
     - If primary is dark, this is light
   - Foreground (constrasts with secondary)
     - If secondary is light, this is dark
     - If secondary is dark, this is light
   - Accent: A colour that is in the input but far from 1/2/3
   - Alert: A colour not in the input as far as possible from 1/2/3
 - Complexity: How 'minimal' an image is
 - Fluidity: How 'sharp' an image is

### Brightness

Brightness is determined by clustering all pixels in the image based on proximity to the pure colours of the basic palette.

If white or black has the largest cluster, then the image is light or dark respectively.

If a colour has the largest cluster, then the image is considered to be 'typical'.

### Warmth

Warmth is determined by clustering the image into four clusters: pure red, pure cyan, pure green-yellow, and pure blue-magenta. If pure red or pure cyan are largest, then the image is considered warm or cool respectively. Otherwise it is considered typical.

### Light and Dark

A reference light and dark colour are found by clustering the image into two clusters. The brighter cluster gets the light value, the darker cluster gets the dark value. During this process the clusters are forbidden from becoming too close to one another in order to ensure contrast is maintained.

### Basic Palette

The basic palette is created by clustering 8 clusters originally seeded as for warmth. Then using delta e 2000 as a metric the clusters are matched with the closest ANSI colour.

If any cluster is too far from the colour it is to represent, it is moved to the nearest colour that is close enough.

If any cluster has no elements at all, then the pure colour it is to represent is used instead.

### ANSI Palette

The process for the ANSI palette is identical to that for the basic palette except that twice as many clusters are used.

### Theme Palette

To calculate a theme palette calculation begins with two clusters, and finds their silouette. When the silouette begins to decrease the algorithm stops and the `n` clusters remaining are used as the `n` theme colours.

### Background and Foreground Colours

Background and foreground colours are chosen to contrast with the primary and secondary colours respectively.

This is done by choosing colours from the theme palette that are as far as possible from the respective target colour.

If there are only two colours in the palette then the background is white if the
image is light and black if the image is dark with the foreground being the
opposite. If the image is typical it is treated as light in this case.

If there are only three colours in the palette then the background is set to the
tertiary colour and the foreground is its RGB complement.

### Accent and Alert Colours

Accent and alert colours are then chosen from the image as the colours that exist in the image but are as far as possible from the theme colours.

Accent is chosen first, then alert is chosen to be far from accent as well.

### Complexity

The complexity of the image is calculated by comparing its bitmap size to its compressed size.

### Fluidity

The fluidity of the image is calculated by running edge detection on the image and seeing how much of the image consists of edges.

## File Generation

File generation is run by a config file with the following whitespace-separated format. Any lines starting with a `#` are comments.

The first column is the source file from which to generate a new file.

The second column is the set of characters representing the start of a meta sequence.

The third column is the set of characters representing the end of a meta sequence.

The remaining column(s) are the name of the resulting file. (Note result files
may contain whitespace but must not begin with whitespace. If a file name does
begin with whitespace you may use an absolute file path to rectify the issue)

For example:

```
# This sets up i3.config to generate i3 configuration files with meta sequences between three curly braces
i3.config {{{ }}} /home/user/.config/i3/config
```

## Meta Sequences

These are the commands used to incorporate the theme into config files.

### Variables

Meta sequences can define variables based on the data derived from the image.

 - `set <name> <value>`: Sets the variable to be equal to a value

Variables are typed as either booleans, integers, numbers, strings, or colours.  Depending on the type, different operators do different things.

Types are implicitly coerced in the following order: Colours -> Integers -> Numbers -> Strings -> Booleans. Thus any operation involving a boolean will always coerce the other to a boolean for example.

### Expressions

These operations are summarized in the following table:

| Operator  | Booleans    | Integers       | Numbers        | Strings              | Colours                 |
|-----------|-------------|----------------|----------------|----------------------|-------------------------|
| `+`       | Logical OR  | Addition       | Addition       | Concatenation        | Additive Combination    |
| `-`       | Logical XOR | Subtraction    | Subtraction    | Levenshtein Distance | Subtractive Combination |
| `*`       | Logical AND | Multiplication | Multiplication | -                    | Average Colour          |
| `~` unary | Negation    | Negative       | Negative       | Toggle case          | Inverse Colour          |

The common comparison operators (`>`, `<`, `>=`, `<=`, `==`, `!=`) are also defined. For colours brighter colours are considered larger than darker ones.

### Built-in Variables

Here is a table summarizing all built-in variables:

| Variable      | Type    | Value |
|---------------|---------|-------|
| format        | String  | Format for colour rendering |
| bright        | Boolean | True if the image is bright, False otherwise |
| dim           | Boolean | True if the image is dark, False otherwise |
| warm          | Boolean | True if the image is warm, False otherwise |
| cool          | Boolean | True if the image is cool, False otherwise |
| light         | Colour  | Average of all the lightest colours in the image |
| dark          | Colour  | Average of all the darkest colours in the image |
| white         | Colour  | White cluster colour |
| black         | Colour  | Black cluster colour |
| red           | Colour  | Red cluster colour |
| green         | Colour  | Green cluster colour |
| blue          | Colour  | Blue cluster colour |
| cyan          | Colour  | Cyan cluster colour |
| magenta       | Colour  | Magenta cluster colour |
| yellow        | Colour  | Yellow cluster colour |
| awhite        | Colour  | ANSI white cluster colour |
| ablack        | Colour  | ANSI black cluster colour |
| ared          | Colour  | ANSI red cluster colour |
| agreen        | Colour  | ANSI green cluster colour |
| ablue         | Colour  | ANSI blue cluster colour |
| acyan         | Colour  | ANSI cyan cluster colour |
| amagenta      | Colour  | ANSI magenta cluster colour |
| ayellow       | Colour  | ANSI yellow cluster colour |
| aWhite        | Colour  | ANSI bright white cluster colour |
| aBlack        | Colour  | ANSI bright black cluster colour |
| aRed          | Colour  | ANSI bright red cluster colour |
| aGreen        | Colour  | ANSI bright green cluster colour |
| aBlue         | Colour  | ANSI bright blue cluster colour |
| aCyan         | Colour  | ANSI bright cyan cluster colour |
| aMagenta      | Colour  | ANSI bright magenta cluster colour |
| aYellow       | Colour  | ANSI bright yellow cluster colour |
| primary       | Colour  | Biggest cluster colour |
| secondary     | Colour  | Second biggest cluster colour |
| tertiary      | Colour  | Third biggest cluster colour |
| quaternary    | Colour  | Fourth biggest cluster colour, or black |
| quinary       | Colour  | Fifth biggest cluster colour, or black |
| senary        | Colour  | Sixth biggest cluster colour, or black |
| quinary       | Colour  | Fifth biggest cluster colour, or black |
| senary        | Colour  | Sixth biggest cluster colour, or black |
| septenary     | Colour  | Seventh biggest cluster colour, or black |
| octonary      | Colour  | Eighth biggest cluster colour, or black |
| nonary        | Colour  | Ninth biggest cluster colour, or black |
| denary        | Colour  | Tenth biggest cluster colour, or black |
| undenary      | Colour  | Eleventh biggest cluster colour, or black |
| duodenary     | Colour  | Twelfth biggest cluster colour, or black |
| tredenary     | Colour  | Thirteenth biggest cluster colour, or black |
| quaterdenary  | Colour  | Fourtheenth biggest cluster colour, or black |
| quidenary     | Colour  | Fifteenth biggest cluster colour, or black |
| sedenary      | Colour  | Sixteenth biggest cluster colour, or black |
| septedenary   | Colour  | Seventeenth biggest cluster colour, or black |
| duodevigenary | Colour  | Eighteenth biggest cluster colour, or black |
| undevigenary  | Colour  | Nineteenth biggest cluster colour, or black |
| vigenary      | Colour  | Twentieth biggest cluster colour, or black |
| background    | Colour  | Background colour |
| foreground    | Colour  | Foreground colour |
| Accent        | Colour  | Accent colour |
| Alert         | Colour  | Alert colour |
| Complexity    | Number  | Ratio of uncompressed size to compressed size of image |
| Fluidity      | Number  | Percentage of image that has an edge detected |
| Image         | String  | Absolute path to copy of image file used to generate colours |

Complexity and Fluidity have corrections applied due to the fact that bigger images will tend to have smaller values due to the nature of images. Thus they store the *average* complexity or fluidity per pixel.

The format variable is described in the values section.

### Literal values

#### Booleans

#### Integers

#### Numbers

#### Strings

#### Colours

### Conditionals

These commands effect whether parts of the file are present at all

 - `if <condition>`: Only include the section if condition is met
 - `always`: Always include the section
 - `never`: Never include the section

### Values

If a meta command results in just a value then that value is inserted in the document verbatim. If it is a colour it is inserted according to the value of the format variable.

The format variable uses a printf like format to describe how colours should be printed.

The default value is "#%R%G%B".

The specifiers are:

| Specier | Meaning | Example for `#C01012` |
|---------|---------|-----------------------|
| `%` | Literal `%` | `%` |
| `R` | Hexadecimal red channel, capitalized | `C0` |
| `G` | Hexadecimal green channel, capitalized | `10` |
| `B` | Hexadecimal blue channel, capitalized | `12` |
| `r` | Hexadecimal red channel, lower case | `c0` |
| `g` | Hexadecimal green channel, lower case | `10` |
| `b` | Hexadecimal blue channel, lower case | `12` |
| `1` | Red channel as decimal byte | `192` |
| `2` | Green channel as decimal byte | `16` |
| `3` | Blue channel as decimal byte | `18` |
| `h` | Hue as decimal degree between 0 and 360 | `359` |
| `H` | Hue as decimal degree between -180 and 180 | `-1` |
| `s` | HSL Saturation as decimal percentage | `85` |
| `S` | HSV Saturation as decimal percentage | `92` |
| `u` | Luminosity as decimal percentage | `41` |
| `v` | Value as decimal percentage | `75` |
| `c` | Cyan as decimal percentage | `0` |
| `m` | Magenta as decimal percentage | `92` |
| `y` | Yellow as decimal percentage | `91` |
| `k` | Black as decimal percentage | `25` |
| `L` | Cie-L as decimal number between 0 and 100 | `40.6068532` |
| `l` | Cie-L as decimal integer between 0 and 100 | `41` |
| `A` | Cie-A as decimal number between -128 and 128 | `63.15204996` |
| `a` | Cie-A as decimal integer between -128 and 128 | `63` |
| `W` | Cie-B as decimal number between -128 and 128 | `48.06535524` |
| `w` | Cie-B as decimal integer between -128 and 128 | `48` |
| `X` | X as decimal number between 0 and 1 | `0.2327` |
| `x` | 100X as decimal integer | `23` |
| `Y` | Y as decimal number between 0 and 1 | `0.1214` |
| `i` | 100Y as decimal integer | `12` |
| `Z` | Z as decimal number between 0 and 1 | `0.0122` |
| `z` | 100Z as decimal integer | `1` |

If necessary the format specifier can be overruled on a case-by-case basis using the format command:

 - `format <format-str> <colour>`: Render the given colour using the given format specifier

## Theme directory

The theme directory supplied to the command should contain three
sub-directories. These should be `current`, `themes`, and `schemes`.

The `current` directory contains the generated files from the currently loaded
theme along with a .theme file with some metadata.

The `themes` directory contains the configuration files for the various themes
that a user creates. Each theme is a directory with a `config` file and the
files said config file references.

The `schemes` directory contains the calculated information about the colour
schemes. Each directory is the name of one scheme. These directories store
a copy of the image used to generate the scheme as well as a hidden `.thimdata`
file describing the statistics calculated from the image.

TODO: Make each file start with {{{ }}} target_file?

with Ada.Strings.Unbounded;

generic
    type Option_Type is (<>);
package AdaArgp is
    -- Usage example:
    -- Option (Colour_Option) ; Short ("c") ; Short ("C") ; Long ("color") ; Long ("-colour") ; Argument ("COLOUR") ; Optional ; Description ("BLAH BLAH BLAH")
    -- Option (Negate_Option) ; Short ("n") ; Long ("negate") ; "BLAH BLAH BLAH"
    procedure Option (Index : Option_Type);
    procedure Short (Name : String);
    procedure Long (Name : String);
    procedure Argument (Name : String);
    procedure Optional;
    procedure Description (Text : String);

    procedure Parse_Arguments;

    function More_Arguments return Boolean;
    function Next_Argument return Option_Type;
    function Parameter return String; -- Empty string if no more parameters
end AdaArgp;

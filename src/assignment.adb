with Ada.Containers.Ordered_Sets;
with Ada.Containers.Ordered_Maps;
with Ada.Containers.Doubly_Linked_Lists;

-- Assign currently uses the O(n^4) variant of the hungarian algorithm using
-- M-augmenting paths on complete bipartite graphs. In future if necessary it
-- may be improved to O(n^3) by known methods.
--
-- Note: it has not yet been proven that this is even O(n^4).
package body Assignment is
    type Node_Kind is (Agent_Node, Job_Node);
    type Node is
        record
            Kind : Node_Kind;
            Id : Positive;
        end record;

    type Assignment is
        record
            Agent, Job : Positive;
        end record;

    function "<" (L, R : Node) return Boolean
        is (if L.Id = R.Id then L.Kind < R.Kind else L.Id < R.Id);
    function "<" (L, R : Assignment) return Boolean
        is (if L.Agent = R.Agent then L.Job < R.Job else L.Agent < R.Agent);

    package Node_Lists is new Ada.Containers.Doubly_Linked_Lists (Node);
    package Node_Sets is new Ada.Containers.Ordered_Sets (Node);
    package Node_Maps is new Ada.Containers.Ordered_Maps (Node, Node);
    package Potential_Maps is new Ada.Containers.Ordered_Maps (Node, Cost);
    package Assignment_Sets is new Ada.Containers.Ordered_Sets (Assignment);

    use type Node_Sets.Set;

    function Assign (Costs : Cost_Matrix) return Assignments is
        -- Invariants:
        --  Potentials (Agent, A) + Potentials (Job, J) <= Costs (A, J)
        --  (A, J) in Matching implies
        --      Potentials (Agent, A) + Potentials (Job, J) = Costs (A, J)
        Potentials : Potential_Maps.Map := Potential_Maps.Empty_Map;
        Matching : Assignment_Sets.Set := Assignment_Sets.Empty_Set;
        All_Agents : Node_Sets.Set := Node_Sets.Empty_Set;
        All_Jobs : Node_Sets.Set := Node_Sets.Empty_Set;

        procedure Initialize_Potentials is begin
            for Agent in Costs'Range (1) loop
                All_Agents.Insert ((Agent_Node, Agent));
                Potentials.Insert ((Agent_Node, Agent), Zero);
            end loop;

            for Job in Costs'Range (2) loop
                All_Jobs.Insert ((Job_Node, Job));
                Potentials.Insert ((Job_Node, Job), Zero);
            end loop;
        end Initialize_Potentials;

        function In_Matching (Check : Node) return Boolean is (
            case Check.Kind is
                when Agent_Node => (
                    for some Job in Costs'Range (2) =>
                        Matching.Contains ((Check.Id, Job))
                ),
                when Job_Node => (
                    for some Agent in Costs'Range (1) =>
                        Matching.Contains ((Agent, Check.Id))
                )
        );

        function Is_Tight (Check : Assignment) return Boolean is (
            (Potentials.Element ((Agent_Node, Check.Agent)) +
            Potentials.Element ((Job_Node, Check.Job))) =
            Costs (Check.Agent, Check.Job)
        );

        function Is_Perfect_Matching return Boolean is begin
            for Agent in Costs'Range (1) loop
                if not In_Matching ((Agent_Node, Agent)) then
                    return False;
                end if;
            end loop;

            for Job in Costs'Range (2) loop
                if not In_Matching ((Job_Node, Job)) then
                    return False;
                end if;
            end loop;

            return True;
        end Is_Perfect_Matching;

        function Uncovered_Agents return Node_Sets.Set is
            Result : Node_Sets.Set := Node_Sets.Empty_Set;
        begin
            for Agent in Costs'Range (1) loop
                if not In_Matching ((Agent_Node, Agent)) then
                    Result.Insert ((Agent_Node, Agent));
                end if;
            end loop;

            return Result;
        end Uncovered_Agents;

        function Uncovered_Jobs return Node_Sets.Set is
            Result : Node_Sets.Set := Node_Sets.Empty_Set;
        begin
            for Job in Costs'Range (2) loop
                if not In_Matching ((Job_Node, Job)) then
                    Result.Insert ((Job_Node, Job));
                end if;
            end loop;

            return Result;
        end Uncovered_Jobs;

        function Reachable_From (Start : Node) return Node_Sets.Set is
            Result : Node_Sets.Set := Node_Sets.Empty_Set;
        begin
            case Start.Kind is
                when Agent_Node =>
                    for Job in Costs'Range (2) loop
                        if not Matching.Contains ((Start.Id, Job)) then
                            if Is_Tight ((Start.Id, Job)) then
                                Result.Insert ((Job_Node, Job));
                            end if;
                        end if;
                    end loop;

                when Job_Node =>
                    for Agent in Costs'Range (1) loop
                        if Matching.Contains ((Agent, Start.Id)) then
                            if Is_Tight ((Agent, Start.Id)) then
                                Result.Insert ((Agent_Node, Agent));
                            end if;
                        end if;
                    end loop;
            end case;

            return Result;
        end Reachable_From;

        procedure Search (
            From : Node_Sets.Set;
            Found : out Node_Sets.Set;
            Parents : out Node_Maps.Map
        ) is
            Queue : Node_Lists.List := Node_Lists.Empty_List;
            Check : Node;
        begin
            Found := From;

            for Source of From loop
                Queue.Append (Source);
            end loop;

            while not Queue.Is_Empty loop
                Check := Queue.First_Element;
                Queue.Delete_First;

                for Adjacent of Reachable_From (Check) loop
                    if not Found.Contains (Adjacent) then
                        Found.Insert (Adjacent);
                        Parents.Insert (Adjacent, Check);
                        Queue.Append (Adjacent);
                    end if;
                end loop;
            end loop;
        end Search;

        procedure Reverse_Orientation (From : Node; To : Node) is
            Edge : Assignment;
        begin
            case From.Kind is
                when Agent_Node =>
                    Edge := (From.Id, To.Id);
                when Job_Node =>
                    Edge := (To.Id, From.Id);
            end case;

            if Matching.Contains (Edge) then
                Matching.Delete (Edge);
            else
                Matching.Insert (Edge);
            end if;
        end Reverse_Orientation;

        procedure Reverse_Path (To : Node; Using : Node_Maps.Map) is
            Current : Node := To;
            Next : Node;
        begin
            while Using.Contains (Current) loop
                Next := Using.Element (Current);
                Reverse_Orientation (Current, Next);
                Current := Next;
            end loop;
        end Reverse_Path;

        procedure Increment_Potentials (From : Node_Sets.Set; By : Cost) is
            Previous : Cost;
        begin
            for Item of From loop
                Previous := Potentials.Element (Item);
                Potentials.Replace (Item, Previous + By);
            end loop;
        end Increment_Potentials;

        procedure Decrement_Potentials (From : Node_Sets.Set; By : Cost) is
            Previous : Cost;
        begin
            for Item of From loop
                Previous := Potentials.Element (Item);
                Potentials.Replace (Item, Previous - By);
            end loop;
        end Decrement_Potentials;

        -- Either increases potentials or adds more edges to matching
        procedure Improve_Solution is
            Accessible : Node_Sets.Set;
            Parents : Node_Maps.Map;
            Open_Jobs : constant Node_Sets.Set := Uncovered_Jobs;
        begin
            Search (Uncovered_Agents, Accessible, Parents);

            if Node_Sets.Overlap (Open_Jobs, Accessible) then
                declare
                    Candidates : constant Node_Sets.Set :=
                        Open_Jobs and Accessible;
                    Candidate : constant Node := Candidates.First_Element;
                begin
                    -- Only need to send parents since open agents dont have any
                    Reverse_Path (Candidate, Parents);
                end;
            else
                declare
                    Usable_Agents : constant Node_Sets.Set :=
                        Accessible and All_Agents;
                    Usable_Jobs : constant Node_Sets.Set :=
                        All_Jobs - Accessible;
                    Del : Cost := Zero;
                    Candidate : Cost;
                    First : Boolean := True;
                begin
                    for Agent of Usable_Agents loop
                        for Job of Usable_Jobs loop
                            Candidate := Costs (Agent.Id, Job.Id);
                            Candidate := @ - Potentials.Element (Agent);
                            Candidate := @ - Potentials.Element (Job);

                            if First or else Candidate < Del then
                                Del := Candidate;
                            end if;

                            First := False;
                        end loop;
                    end loop;

                    Increment_Potentials (Usable_Agents, Del);
                    Decrement_Potentials (Accessible and All_Jobs, Del);
                end;
            end if;
        end Improve_Solution;

        function Solution_Result return Assignments is
            Result : Assignments (Costs'First (1) .. Costs'Last (1));
        begin
            for Agent in Result'Range loop
                for Job in Costs'Range (2) loop
                    if Matching.Contains ((Agent, Job)) then
                        Result (Agent) := Job;
                        exit;
                    end if;
                end loop;
            end loop;

            return Result;
        end Solution_Result;

        Give_Up_Counter : Natural := Costs'Length (1) * Costs'Length (2);
    begin
        Give_Up_Counter := @ * @;
        Initialize_Potentials;

        while not Is_Perfect_Matching loop
            Improve_Solution;
            Give_Up_Counter := @ - 1;

            if Give_Up_Counter = 0 then
                -- This typically occurs when using floating point types or
                -- other types where arithmetic may lead to a + b - b - a /= 0
                raise Constraint_Error;
            end if;
        end loop;

        return Solution_Result;
    end Assign;
end Assignment;

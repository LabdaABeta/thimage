generic
    type Cost is private;
    Zero : Cost;

    with function "+" (L, R : Cost) return Cost is <>;
    with function "-" (L, R : Cost) return Cost is <>;
    with function "<" (L, R : Cost) return Boolean is <>;
    with function "=" (L, R : Cost) return Boolean is <>;
package Assignment is
    -- Matrix of (Agent, Task) -> Cost
    type Cost_Matrix is array (Positive range <>, Positive range <>) of Cost;

    -- Array of (Agent) -> Task
    type Assignments is array (Positive range <>) of Positive;

    function Assign (Costs : Cost_Matrix) return Assignments;
end Assignment;

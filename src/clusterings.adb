with Logging;
with Ada.Numerics.Float_Random;

package body Clusterings is
    function Cluster (Elements : Element_Array; Sources : Element_Array)
        return Cluster_Result
    is
        Result : Cluster_Result := (
            Size => Elements'Length,
            Count => Sources'Length,
            Data => Elements,
            Membership => (others => 1),
            References => Sources,
            Distance_Sums => (others => Zero),
            Score => Zero
        );

        Test : Length;
        Candidate : Positive;
        Dist : Length := Zero;
        First : Boolean;
    begin
        for Element_Index in Elements'Range loop
            First := True;

            for Cluster_Index in Sources'Range loop
                Test := Distance (
                    Elements (Element_Index), Sources (Cluster_Index)
                );

                if First or else Test < Dist then
                    Candidate := Cluster_Index;
                    Dist := Test;
                end if;

                First := False;
            end loop;

            Result.Membership (Element_Index) := Candidate;
            Result.Distance_Sums (Candidate) := @ + Dist;
            Result.Score := @ + Dist;
        end loop;

        return Result;
    end Cluster;

    function Get_Cluster (From : Cluster_Result; Index : Positive)
        return Element_Array
    is
        Result : Element_Array (1 .. From.Size);
        Next : Natural := 1;
    begin
        for I in From.Data'Range loop
            if From.Membership (I) = Index then
                Result (Next) := From.Data (I);
                Next := @ + 1;
            end if;
        end loop;

        return Result (1 .. Next - 1);
    end Get_Cluster;

    function Get_Reference (From : Cluster_Result; Index : Positive)
        return Element is (From.References (Index));

    function Get_Distance_Sum (From : Cluster_Result; Index : Positive)
        return Length is (From.Distance_Sums (Index));

    function Get_Cluster_Size (From : Cluster_Result; Index : Positive)
        return Natural
    is
        Result : Natural := 0;
    begin
        for I in From.Data'Range loop
            if From.Membership (I) = Index then
                Result := @ + 1;
            end if;
        end loop;

        return Result;
    end Get_Cluster_Size;

    package body Silhouettes is
        Generator : Ada.Numerics.Float_Random.Generator;

        function Assignment_Fit (
            From : Cluster_Result;
            Target : Positive
        ) return Length is
            Relevant_Cluster : constant Positive := From.Membership (Target);
            Cohabitants : constant Natural :=
                Get_Cluster_Size (From, Relevant_Cluster);
            Distances : Distance_Array (1 .. Cohabitants - 1);
            Next_Data_Index : Positive := 1;
        begin
            for I in Distances'Range loop
                while
                    Next_Data_Index = Target or
                    From.Membership (Next_Data_Index) /= Relevant_Cluster
                loop
                    Next_Data_Index := @ + 1;
                end loop;

                Distances (I) :=
                    Distance (From.Data (Target), From.Data (Next_Data_Index));
            end loop;

            return Mean (Distances);
        end Assignment_Fit;

        function Mean_Dissimilarity (
            From : Cluster_Result;
            Target : Positive;
            Cluster : Element_Array
        ) return Length is
            Distances : Distance_Array (Cluster'First .. Cluster'Last);
            Reference : constant Element := From.Data (Target);
        begin
            for I in Distances'Range loop
                Distances (I) := Distance (Reference, Cluster (I));
            end loop;

            return Mean (Distances);
        end Mean_Dissimilarity;

        function Element_Silhouette (
            From : Cluster_Result;
            Target : Positive
        ) return Length is
            Neighbor : Length := Zero;
            First : Boolean := True;
            Fit : constant Length := Assignment_Fit (From, Target);
            Candidate : Length;
        begin
            if Get_Cluster_Size (From, From.Membership (Target)) < 2 then
                return Zero;
            end if;

            for Cluster_Index in From.References'Range loop
                if Cluster_Index /= From.Membership (Target) then
                    Candidate :=
                        Mean_Dissimilarity (
                            From, Target, Get_Cluster (From, Cluster_Index));
                    if First or else Candidate < Neighbor then
                        Neighbor := Candidate;
                    end if;

                    First := False;
                end if;
            end loop;

            if (if Fit < Neighbor then Neighbor else Fit) = Zero then
                return Zero;
            else
                return (Neighbor - Fit) /
                    (if Fit < Neighbor then Neighbor else Fit);
            end if;
        end Element_Silhouette;

        function Silhouette (From : Cluster_Result) return Length is
            Results : Distance_Array (1 .. From.Size);
        begin
            for I in Results'Range loop
                Logging.Log_Part ("Processing element" &
                    I'Img & " of" & Results'Last'Img & "...");
                Results (I) := Element_Silhouette (From, I);
                Logging.Log (" DONE");
            end loop;

            return Mean (Results);
        end Silhouette;

        function Approximate_Silhouette (
            From : Cluster_Result;
            Samples : Positive
        ) return Length is
            Results : Distance_Array (1 .. Samples);
            Index : Natural := 0;
        begin
            for I in Results'Range loop
                while Index = 0 loop
                    Index := Natural (Float (From.Size) *
                        Ada.Numerics.Float_Random.Random (Generator));
                end loop;

                Results (I) := Element_Silhouette (From, Index);
                Index := 0;
            end loop;

            return Mean (Results);
        end Approximate_Silhouette;
    end Silhouettes;
end Clusterings;

generic
    type Element is private;
    type Length is private;
    -- Zero < X must be true for all distances except between identical elements
    Zero : Length;

    with function Distance (A, B : Element) return Length;
    with function "+" (A, B : Length) return Length is <>;
    with function "<" (A, B : Length) return Boolean is <>;
package Clusterings is
    type Element_Array is array (Positive range <>) of Element;
    type Membership_Array is array (Positive range <>) of Positive;
    type Distance_Array is array (Positive range <>) of Length;

    type Cluster_Result (Size, Count : Natural) is
        record
            Data : Element_Array (1 .. Size);
            Membership : Membership_Array (1 .. Size);

            References : Element_Array (1 .. Count);
            Distance_Sums : Distance_Array (1 .. Count);

            Score : Length;
        end record;

    function Cluster (Elements : Element_Array; Sources : Element_Array)
        return Cluster_Result;

    function Get_Cluster (From : Cluster_Result; Index : Positive)
        return Element_Array;

    function Get_Reference (From : Cluster_Result; Index : Positive)
        return Element;

    function Get_Distance_Sum (From : Cluster_Result; Index : Positive)
        return Length;

    function Get_Cluster_Size (From : Cluster_Result; Index : Positive)
        return Natural;

    generic
        with function Mean (Lengths : Distance_Array) return Length;
        with function "-" (A, B : Length) return Length is <>;
        with function "/" (A, B : Length) return Length is <>;
    package Silhouettes is
        function Silhouette (From : Cluster_Result) return Length;

        function Approximate_Silhouette (
            From : Cluster_Result;
            Samples : Positive
        ) return Length;
    end Silhouettes;
end Clusterings;

with Colours; use Colours;

package body Colour_Printing is
    UHex_Digits : constant array (RGB_Component range 0 .. 15) of Character := (
        "0123456789ABCDEF");
    LHex_Digits : constant array (RGB_Component range 0 .. 15) of Character := (
        "0123456789abcdef");

    function Upper_Hex (From : RGB_Component) return String is (
        UHex_Digits (From / 16) & UHex_Digits (From mod 16));

    function Lower_Hex (From : RGB_Component) return String is (
        LHex_Digits (From / 16) & LHex_Digits (From mod 16));

    -- SL = space-less
    function SL_Int (From : Integer) return String is
        Result : constant String := Integer'Image (From);
    begin
        if Result (Result'First) = ' ' then
            return Result (Result'First + 1 .. Result'Last);
        else
            return Result;
        end if;
    end SL_Int;

    function SL_Num (From : Float) return String is
        Result : constant String := Float'Image (From);
    begin
        if Result (Result'First) = ' ' then
            return Result (Result'First + 1 .. Result'Last);
        else
            return Result;
        end if;
    end SL_Num;

    function Print (Format : String; From : Colours.Colour) return String is
    begin
        if Format'Length = 0 then
            return "";
        end if;

        if Format (Format'First) /= '%' then
            return Format (Format'First) &
                Print (Format (Format'First + 1 .. Format'Last), From);
        end if;

        if Format'Length = 1 then
            return "%";
        end if;

        return (
            case Format (Format'First + 1) is
                when '%' => "%",
                when 'R' => Upper_Hex (RGB (From).R),
                when 'G' => Upper_Hex (RGB (From).G),
                when 'B' => Upper_Hex (RGB (From).B),
                when 'r' => Lower_Hex (RGB (From).R),
                when 'g' => Lower_Hex (RGB (From).G),
                when 'b' => Lower_Hex (RGB (From).B),
                when '1' => SL_Int (Integer (RGB (From).R)),
                when '2' => SL_Int (Integer (RGB (From).G)),
                when '3' => SL_Int (Integer (RGB (From).B)),
                when 'h' => SL_Int (Integer (HSL (From).H)),
                when 'H' => SL_Int (Integer (HSL (From).H - 180.0)),
                when 's' => SL_Int (Integer (HSL (From).S)),
                when 'S' => SL_Int (Integer (HSV (From).S)),
                when 'u' => SL_Int (Integer (HSL (From).L)),
                when 'v' => SL_Int (Integer (HSV (From).V)),
                when 'c' => SL_Int (Integer (CMYK (From).C)),
                when 'm' => SL_Int (Integer (CMYK (From).M)),
                when 'y' => SL_Int (Integer (CMYK (From).Y)),
                when 'k' => SL_Int (Integer (CMYK (From).K)),
                when 'L' => SL_Num (Float (LAB (From).L)),
                when 'l' => SL_Int (Integer (LAB (From).L)),
                when 'A' => SL_Num (Float (LAB (From).A)),
                when 'a' => SL_Int (Integer (LAB (From).A)),
                when 'W' => SL_Num (Float (LAB (From).B)),
                when 'w' => SL_Int (Integer (LAB (From).B)),
                when 'X' => SL_Num (Float (XYZ (From).X)),
                when 'x' => SL_Int (Integer (100.0 * Float (XYZ (From).X))),
                when 'Y' => SL_Num (Float (XYZ (From).Y)),
                when 'i' => SL_Int (Integer (100.0 * Float (XYZ (From).Y))),
                when 'Z' => SL_Num (Float (XYZ (From).Z)),
                when 'z' => SL_Int (Integer (100.0 * Float (XYZ (From).Z))),
                when others => Format (Format'First + 1) & ""
            ) & Print (Format (Format'First + 2 .. Format'Last), From);
    end Print;

end Colour_Printing;

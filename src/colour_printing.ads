with Colours;

package Colour_Printing is
    function Print (Format : String; From : Colours.Colour) return String;
end Colour_Printing;

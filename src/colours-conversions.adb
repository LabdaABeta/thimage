separate (Colours)
package body Conversions is
    package Colour_Float_Functions is
        new Ada.Numerics.Generic_Elementary_Functions (Colours.Colour_Float);
    use Colour_Float_Functions;

    -- Pre-computed sRGB linearizations
    sRGB_Linearization : constant array (RGB_Component) of Colour_Float := (
    0.000000000000000000000, 0.000303526983548837515, 0.000607053967097675030,
    0.000910580950646512491, 0.00121410793419535006, 0.00151763491774418763,
    0.00182116190129302498, 0.00212468888484186255, 0.00242821586839070012,
    0.00273174285193953769, 0.00303526983548837526, 0.00334653576389915951,
    0.00367650732404743589, 0.00402471701849630662, 0.00439144203741029509,
    0.00477695348069372919, 0.00518151670233838683, 0.00560539162420272286,
    0.00604883302285705391, 0.00651209079259447345, 0.00699541018726538687,
    0.00749903204322617100, 0.00802319298538499426, 0.00856812561806930342,
    0.00913405870222078718, 0.00972121732023784393, 0.0103298230296269417,
    0.0109600940064882458, 0.0116122451797438849, 0.0122864883569158718,
    0.0129830323421730124, 0.0137020830472896864, 0.0144438435960925447,
    0.0152085144229127094, 0.0159962933655096312, 0.0168073757528873838,
    0.0176419544883840776, 0.0185002201283796970, 0.0193823609569357298,
    0.0202885630566524006, 0.0212190103760035616, 0.0221738847933873814,
    0.0231533661781104100, 0.0241576324485047560, 0.0251868596273616303,
    0.0262412218948499046, 0.0273208916390748936, 0.0284260395044208004,
    0.0295568344378088002, 0.0307134437329936345, 0.0318960330730115177,
    0.0331047665708850553, 0.0343398068086821773, 0.0356013148750203359,
    0.0368894504011000185, 0.0382043715953464813, 0.0395462352767328371,
    0.0409151969068531698, 0.0423114106208096544, 0.0437350292569734650,
    0.0451862043856755408, 0.0466650863368800947, 0.0481718242268893981,
    0.0497065659841272323, 0.0512694583740432169, 0.0528606470231802461,
    0.0544802764424423686, 0.0561284900496000910, 0.0578054301910672017,
    0.0595112381629811713, 0.0612460542316176082, 0.0630100176531676742,
    0.0648032666929057449, 0.0666259386437728640, 0.0684781698444001663,
    0.0703600956965958757, 0.0722718506823174789, 0.0742135683801495999,
    0.0761853814813078095, 0.0781874218051863273, 0.0802198203144683236,
    0.0822827071298147944, 0.0843762115441487881, 0.0865004620365497356,
    0.0886555862857729415, 0.0908417111834076835, 0.0930589628466874236,
    0.0953074666309646629, 0.0975873471418624155, 0.0998987282471138910,
    0.102241733088101319, 0.104616484091104162, 0.107023102978267587,
    0.109461710778299331, 0.111932427836905601, 0.114435373826973733,
    0.116970667758510796, 0.119538427988345589, 0.122138772229601872,
    0.124771817560950488, 0.127437680435647405, 0.130136476690364267,
    0.132868321553817920, 0.135633329655205664, 0.138431615032451827,
    0.141263291140271613, 0.144128470858057717, 0.147027266497594983,
    0.149959789810608562, 0.152926151996150173, 0.155926463707827367,
    0.158960835060880379, 0.162029375639110990, 0.165132194501667606,
    0.168269400189690693, 0.171441100732822538, 0.174647403655584982,
    0.177888415983629117, 0.181164244249860162, 0.184474994500440886,
    0.187820772300677757, 0.191201682740791384, 0.194617830441575795,
    0.198069319559948859, 0.201556253794397067, 0.205078736390316929,
    0.208636870145255754, 0.212230757414055116, 0.215860500113899151,
    0.219526199729269206, 0.223227957316808390, 0.226965873510098365,
    0.230740048524349012, 0.234550582161005106, 0.238397573812271002,
    0.242281122465554749, 0.246201326707835344, 0.250158284729953329,
    0.254152094330826639, 0.258182852921595818, 0.262250657529696118,
    0.266355604802862300, 0.270497791013065814, 0.274677312060384538,
    0.278894263476810289, 0.283148740429991941, 0.287440837726917364,
    0.291770649817535865, 0.296138270798321002, 0.300543794415776389,
    0.304987314069886162, 0.309468922817508374, 0.313988713375717543,
    0.318546778125091690, 0.323143209112950580, 0.327778098056542067,
    0.332451536346179188, 0.337163615048330367, 0.341914424908660808,
    0.346704056355029433, 0.351532599500439247, 0.356400144145943398,
    0.361306779783509502, 0.366252595598839326, 0.371237680474148957,
    0.376262122990906334, 0.381326011432529977, 0.386429433787048859,
    0.391572477749723091, 0.396755230725626740, 0.401977779832195625,
    0.407240211901736537, 0.412542613483903586, 0.417885070848137308,
    0.423267669986071515, 0.428690496613906624, 0.434153636174748780,
    0.439657173840918625, 0.445201194516227694, 0.450785782838223292,
    0.456411023180404662, 0.462076999654406906, 0.467783796112158812,
    0.473531496148009379, 0.479320183100826636, 0.485149940056070372,
    0.491020849847835394, 0.496932995060870242, 0.502886458032568484,
    0.508881320854933539, 0.514917665376521394, 0.520995573204354079,
    0.527115125705812870, 0.533276404010505023, 0.539479489012106961,
    0.545724461370186376, 0.552011401512000011, 0.558340389634267686,
    0.564711505704929007, 0.571124829464872863, 0.577580440429650399,
    0.584078417891163992, 0.590618840919336696, 0.597201788363763142,
    0.603827338855337570, 0.610495570807864651, 0.617206562419650884,
    0.623960391675075887, 0.630757136346146829, 0.637596873994032420,
    0.644479681970581919, 0.651405637419823935, 0.658374817279448243,
    0.665387298282272055, 0.672443156957687305, 0.679542469633093615,
    0.686685312435313278, 0.693871761291989686, 0.701101891932973120,
    0.708375779891686652, 0.715693500506480507, 0.723055128921969104,
    0.730460740090353444, 0.737910408772730619, 0.745404209540387219,
    0.752942216776077644, 0.760524504675292201, 0.768151147247506771,
    0.775822218317423373, 0.783537791526193295, 0.791297940332630012,
    0.799102738014408787, 0.806952257669251383, 0.814846572216101017,
    0.822785754396283320, 0.830769876774654414, 0.838799011740740008,
    0.846873231509857716, 0.854992608124233500, 0.863157213454102124,
    0.871367119198796947, 0.879622396887831726, 0.887923117881966317,
    0.896269353374266387, 0.904661174391149236, 0.913098651793418981,
    0.921581856277294387, 0.930110858375423510, 0.938685728457887780,
    0.947306536733199644, 0.955973353249285895, 0.964686247894464777,
    0.973445290398412544, 0.982250550333117145, 0.991102097113829794,
    1.000000000000000000);

    function Float_To_XYZ (From : Colour_Float) return XYZ_Component is (
        if From < Colour_Float (XYZ_Component'First) then
            XYZ_Component'First
        elsif From > Colour_Float (XYZ_Component'Last) then
            XYZ_Component'Last
        else
            XYZ_Component (From));

    function CIEF (T : Colour_Float) return Colour_Float is (
        if T > 0.008856 then
            T ** (1.0 / 3.0)
        else
            7.787 * T + (4.0 / 29.0));

    function CIE_F (T : Colour_Float) return Colour_Float is (
        if T * T * T > 0.008856 then
            T * T * T
        else
            (T - 4.0 / 29.0) / 7.787);

    function Delinearize (T : Colour_Float) return Colour_Float is (
        if T > 0.0031308 then
            1.055 * (T ** (1.0 / 2.4)) - 0.055
        else
            T * 12.92);

    function RGB_To_XYZ (From : RGB_Colour) return XYZ_Colour is
        -- Linear Inputs
        sR : constant Colour_Float := sRGB_Linearization (From.R);
        sG : constant Colour_Float := sRGB_Linearization (From.G);
        sB : constant Colour_Float := sRGB_Linearization (From.B);

        -- XYZ
        X : constant Colour_Float := sR * 0.4124 + sG * 0.3576 + sB * 0.1805;
        Y : constant Colour_Float := sR * 0.2126 + sG * 0.7152 + sB * 0.0722;
        Z : constant Colour_Float := sR * 0.0193 + sG * 0.1192 + sB * 0.9505;
    begin
        return (Float_To_XYZ (X), Float_To_XYZ (Y), Float_To_XYZ (Z));
    end RGB_To_XYZ;

    function XYZ_To_LAB (From : XYZ_Colour) return LAB_Colour is
        -- Normalized XYZ
        NX : constant Colour_Float := Colour_Float (From.X / 0.95047);
        NY : constant Colour_Float := Colour_Float (From.Y);
        NZ : constant Colour_Float := Colour_Float (From.Z / 1.08883);

        -- CIE XYZ
        cX : constant Colour_Float := CIEF (NX);
        cY : constant Colour_Float := CIEF (NY);
        cZ : constant Colour_Float := CIEF (NZ);

        -- LAB
        L : constant Colour_Float := 116.0 * cY - 16.0;
        A : constant Colour_Float := 500.0 * (cX - cY);
        B : constant Colour_Float := 200.0 * (cY - cZ);
    begin
        return (LAB_Component (L), LAB_Component (A), LAB_Component (B));
    end XYZ_To_LAB;

    function LAB_To_XYZ (From : LAB_Colour) return XYZ_Colour is
        -- CIE XYZ
        cY : constant Colour_Float := (Colour_Float (From.L) + 16.0) / 116.0;
        cX : constant Colour_Float := (Colour_Float (From.A) / 500.0) + cY;
        cZ : constant Colour_Float := cY - (Colour_Float (From.B) / 200.0);

        -- Normalized XYZ
        NX : constant Colour_Float := CIE_F (cX);
        NY : constant Colour_Float := CIE_F (cY);
        NZ : constant Colour_Float := CIE_F (cZ);

        -- XYZ
        X : constant Colour_Float := NX * 0.95047;
        Y : constant Colour_Float := NY;
        Z : constant Colour_Float := NZ * 1.08883;
    begin
        return (Float_To_XYZ (X), Float_To_XYZ (Y), Float_To_XYZ (Z));
    end LAB_To_XYZ;

    function XYZ_To_RGB (From : XYZ_Colour) return RGB_Colour is
        -- Linear Inputs
        sR : constant Colour_Float :=
            Colour_Float (From.X) * 3.2406 -
            Colour_Float (From.Y) * 1.5372 -
            Colour_Float (From.Z) * 0.4986;
        sG : constant Colour_Float :=
            Colour_Float (From.Y) * 1.8758 -
            Colour_Float (From.X) * 0.9689 -
            Colour_Float (From.Z) * 0.0415;
        sB : constant Colour_Float :=
            Colour_Float (From.Z) * 1.0570 +
            Colour_Float (From.X) * 0.0557 -
            Colour_Float (From.Y) * 0.2040;

        -- RGB Raw
        RR : constant Colour_Float := Delinearize (sR) * 255.0;
        RG : constant Colour_Float := Delinearize (sG) * 255.0;
        RB : constant Colour_Float := Delinearize (sB) * 255.0;

        -- RGB
        R : constant Colour_Float := (
            if RR < 0.0 then 0.0 elsif RR > 255.0 then 255.0 else RR
        );
        G : constant Colour_Float := (
            if RG < 0.0 then 0.0 elsif RG > 255.0 then 255.0 else RG
        );
        B : constant Colour_Float := (
            if RB < 0.0 then 0.0 elsif RB > 255.0 then 255.0 else RB
        );
    begin
        return (RGB_Component (R), RGB_Component (G), RGB_Component (B));
    end XYZ_To_RGB;

    function HSL_To_RGB (From : HSL_Colour) return RGB_Colour is
        S : constant Colour_Float := Colour_Float (From.S) / 100.0;
        L : constant Colour_Float := Colour_Float (From.L) / 100.0;

        Q : constant Colour_Float := (
            if From.L < 50.0 then
                L * (1.0 + S)
            else
                L + S - L * S
        );

        P : constant Colour_Float := 2.0 * L - Q;

        function Hue_To_RGB (From : Angle) return Colour_Float is (
            if From < 60.0 then
                P + (Q - P) * 6.0 * (Colour_Float (From) / 360.0)
            elsif From < 180.0 then
                Q
            elsif From < 240.0 then
                P + (Q - P) * 6.0 * ((240.0 - Colour_Float (From)) / 360.0)
            else
                P
        );
    begin
        if From.S = 0.0 then
            return (
                R => RGB_Component (255.0 * L),
                G => RGB_Component (255.0 * L),
                B => RGB_Component (255.0 * L)
            );
        else
            return (
                R => RGB_Component (255.0 * Hue_To_RGB (From.H + 120.0)),
                G => RGB_Component (255.0 * Hue_To_RGB (From.H)),
                B => RGB_Component (255.0 * Hue_To_RGB (From.H - 120.0))
            );
        end if;
    end HSL_To_RGB;

    function RGB_To_HSL (From : RGB_Colour) return HSL_Colour is
        R : constant Colour_Float := Colour_Float (From.R) / 255.0;
        G : constant Colour_Float := Colour_Float (From.G) / 255.0;
        B : constant Colour_Float := Colour_Float (From.B) / 255.0;

        Min : constant Colour_Float :=
            Colour_Float'Min (Colour_Float'Min (R, G), B);
        Max : constant Colour_Float :=
            Colour_Float'Max (Colour_Float'Max (R, G), B);

        L : constant Colour_Float := (Min + Max) / 2.0;
        S : constant Colour_Float := (
            if Min = Max then 0.0 else (
                if L < 0.5 then
                    (Max - Min) / (Max + Min)
                else
                    (Max - Min) / (2.0 - Max - Min)));
        H : Colour_Float := 60.0 * (
            if Min = Max then 0.0 else (
                if R > G then (
                    if R > B then -- Red is biggest
                        (G - B) / (Max - Min)
                    else -- Blue is biggest
                        4.0 + (R - G) / (Max - Min))
                else (
                    if G > B then -- Green is biggest
                        2.0 + (B - R) / (Max - Min)
                    else -- Blue is biggest
                        4.0 + (R - G) / (Max - Min))));
    begin
        if H < 0.0 then
            H := H + 360.0;
        end if;

        return (Angle (H), Percentage (100.0 * S), Percentage (100.0 * L));
    end RGB_To_HSL;

    function HSV_To_HSL (From : HSV_Colour) return HSL_Colour is
        SV : constant Colour_Float := Colour_Float (From.S) / 100.0;
        V : constant Colour_Float := Colour_Float (From.V) / 100.0;
        L : constant Colour_Float := V * (1.0 - SV / 2.0);
        SL : constant Colour_Float := (
            if L = 0.0 or L = 1.0 then 0.0 else
                (V - L) / Colour_Float'Min (L, 1.0 - L));
    begin
        return (From.H, Percentage (100.0 * SL), Percentage (100.0 * L));
    end HSV_To_HSL;

    function HSL_To_HSV (From : HSL_Colour) return HSV_Colour is
        SL : constant Colour_Float := Colour_Float (From.S) / 100.0;
        L : constant Colour_Float := Colour_Float (From.L) / 100.0;
        V : constant Colour_Float := L + SL * Colour_Float'Min (L, 1.0 - L);
        SV : constant Colour_Float := (
            if V = 0.0 then 0.0 else
                2.0 * (1.0 - (L / V)));
    begin
        return (From.H, Percentage (100.0 * SV), Percentage (100.0 * V));
    end HSL_To_HSV;

    function CMYK_To_RGB (From : CMYK_Colour) return RGB_Colour is
        C : constant Colour_Float := Colour_Float (From.C) / 100.0;
        M : constant Colour_Float := Colour_Float (From.M) / 100.0;
        Y : constant Colour_Float := Colour_Float (From.Y) / 100.0;
        K : constant Colour_Float := Colour_Float (From.K) / 100.0;

        R : constant Colour_Float := 255.0 * (1.0 - C) * (1.0 - K);
        G : constant Colour_Float := 255.0 * (1.0 - M) * (1.0 - K);
        B : constant Colour_Float := 255.0 * (1.0 - Y) * (1.0 - K);
    begin
        return (RGB_Component (R), RGB_Component (G), RGB_Component (B));
    end CMYK_To_RGB;

    function RGB_To_CMYK (From : RGB_Colour) return CMYK_Colour is
        R : constant Colour_Float := Colour_Float (From.R) / 255.0;
        G : constant Colour_Float := Colour_Float (From.G) / 255.0;
        B : constant Colour_Float := Colour_Float (From.B) / 255.0;

        K : constant Colour_Float := 1.0 - Colour_Float'Max (
            Colour_Float'Max (R, G), B);
        C : constant Colour_Float := (if K = 1.0 then 0.0 else
            (1.0 - R - K) / (1.0 - K));
        M : constant Colour_Float := (if K = 1.0 then 0.0 else
            (1.0 - G - K) / (1.0 - K));
        Y : constant Colour_Float := (if K = 1.0 then 0.0 else
            (1.0 - B - K) / (1.0 - K));
    begin
        return (Percentage (C), Percentage (M), Percentage (Y), Percentage (K));
    end RGB_To_CMYK;
end Conversions;

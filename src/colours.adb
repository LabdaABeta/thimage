with Ada.Numerics.Generic_Elementary_Functions;

package body Colours is
    use type Ada.Containers.Hash_Type;

    package Conversions is
        function RGB_To_XYZ (From : RGB_Colour) return XYZ_Colour;
        function XYZ_To_LAB (From : XYZ_Colour) return LAB_Colour;
        function LAB_To_XYZ (From : LAB_Colour) return XYZ_Colour;
        function XYZ_To_RGB (From : XYZ_Colour) return RGB_Colour;
        function HSL_To_RGB (From : HSL_Colour) return RGB_Colour;
        function RGB_To_HSL (From : RGB_Colour) return HSL_Colour;
        function HSV_To_HSL (From : HSV_Colour) return HSL_Colour;
        function HSL_To_HSV (From : HSL_Colour) return HSV_Colour;
        function CMYK_To_RGB (From : CMYK_Colour) return RGB_Colour;
        function RGB_To_CMYK (From : RGB_Colour) return CMYK_Colour;
    end Conversions;

    package body Conversions is separate;

    function "+" (L, R : Angle) return Angle is
        Result : Colour_Float := Colour_Float (L) + Colour_Float (R);
    begin
        while Result > 360.0 loop
            Result := @ - 360.0;
        end loop;

        while Result < 0.0 loop
            Result := @ + 360.0;
        end loop;

        return Angle (Result);
    end "+";

    function "-" (L, R : Angle) return Angle is
        Result : Colour_Float := Colour_Float (L) - Colour_Float (R);
    begin
        while Result > 360.0 loop
            Result := @ - 360.0;
        end loop;

        while Result < 0.0 loop
            Result := @ + 360.0;
        end loop;

        return Angle (Result);
    end "-";

    function Create (From : RGB_Colour) return Colour is
        XYZ_Part : constant XYZ_Colour := Conversions.RGB_To_XYZ (From);
        LAB_Part : constant LAB_Colour := Conversions.XYZ_To_LAB (XYZ_Part);
        HSL_Part : constant HSL_Colour := Conversions.RGB_To_HSL (From);
        HSV_Part : constant HSV_Colour := Conversions.HSL_To_HSV (HSL_Part);
        CMYK_Part : constant CMYK_Colour := Conversions.RGB_To_CMYK (From);
    begin
        return (From, LAB_Part, XYZ_Part, HSL_Part, HSV_Part, CMYK_Part);
    end Create;

    function Create (From : LAB_Colour) return Colour is
        XYZ_Part : constant XYZ_Colour := Conversions.LAB_To_XYZ (From);
        RGB_Part : constant RGB_Colour := Conversions.XYZ_To_RGB (XYZ_Part);
        HSL_Part : constant HSL_Colour := Conversions.RGB_To_HSL (RGB_Part);
        HSV_Part : constant HSV_Colour := Conversions.HSL_To_HSV (HSL_Part);
        CMYK_Part : constant CMYK_Colour := Conversions.RGB_To_CMYK (RGB_Part);
    begin
        return (RGB_Part, From, XYZ_Part, HSL_Part, HSV_Part, CMYK_Part);
    end Create;

    function Create (From : XYZ_Colour) return Colour is
        LAB_Part : constant LAB_Colour := Conversions.XYZ_To_LAB (From);
        RGB_Part : constant RGB_Colour := Conversions.XYZ_To_RGB (From);
        HSL_Part : constant HSL_Colour := Conversions.RGB_To_HSL (RGB_Part);
        HSV_Part : constant HSV_Colour := Conversions.HSL_To_HSV (HSL_Part);
        CMYK_Part : constant CMYK_Colour := Conversions.RGB_To_CMYK (RGB_Part);
    begin
        return (RGB_Part, LAB_Part, From, HSL_Part, HSV_Part, CMYK_Part);
    end Create;

    function Create (From : HSL_Colour) return Colour is
        RGB_Part : constant RGB_Colour := Conversions.HSL_To_RGB (From);
        XYZ_Part : constant XYZ_Colour := Conversions.RGB_To_XYZ (RGB_Part);
        LAB_Part : constant LAB_Colour := Conversions.XYZ_To_LAB (XYZ_Part);
        HSV_Part : constant HSV_Colour := Conversions.HSL_To_HSV (From);
        CMYK_Part : constant CMYK_Colour := Conversions.RGB_To_CMYK (RGB_Part);
    begin
        return (RGB_Part, LAB_Part, XYZ_Part, From, HSV_Part, CMYK_Part);
    end Create;

    function Create (From : HSV_Colour) return Colour is
        HSL_Part : constant HSL_Colour := Conversions.HSV_To_HSL (From);
        RGB_Part : constant RGB_Colour := Conversions.HSL_To_RGB (HSL_Part);
        XYZ_Part : constant XYZ_Colour := Conversions.RGB_To_XYZ (RGB_Part);
        LAB_Part : constant LAB_Colour := Conversions.XYZ_To_LAB (XYZ_Part);
        CMYK_Part : constant CMYK_Colour := Conversions.RGB_To_CMYK (RGB_Part);
    begin
        return (RGB_Part, LAB_Part, XYZ_Part, HSL_Part, From, CMYK_Part);
    end Create;

    function Create (From : CMYK_Colour) return Colour is
        RGB_Part : constant RGB_Colour := Conversions.CMYK_To_RGB (From);
        XYZ_Part : constant XYZ_Colour := Conversions.RGB_To_XYZ (RGB_Part);
        LAB_Part : constant LAB_Colour := Conversions.XYZ_To_LAB (XYZ_Part);
        HSL_Part : constant HSL_Colour := Conversions.RGB_To_HSL (RGB_Part);
        HSV_Part : constant HSV_Colour := Conversions.HSL_To_HSV (HSL_Part);
    begin
        return (RGB_Part, LAB_Part, XYZ_Part, HSL_Part, HSV_Part, From);
    end Create;

    function RGB (From : Colour) return RGB_Colour is (From.RGB_Part);
    function LAB (From : Colour) return LAB_Colour is (From.LAB_Part);
    function XYZ (From : Colour) return XYZ_Colour is (From.XYZ_Part);
    function HSL (From : Colour) return HSL_Colour is (From.HSL_Part);
    function HSV (From : Colour) return HSV_Colour is (From.HSV_Part);
    function CMYK (From : Colour) return CMYK_Colour is (From.CMYK_Part);

    function RGB_Complement (From : Colour) return Colour is (
        Create (
            RGB_Colour'(
                255 - From.RGB_Part.R,
                255 - From.RGB_Part.G,
                255 - From.RGB_Part.B
            )
        )
    );

    function Hash (From : Colour) return Ada.Containers.Hash_Type is (
        Ada.Containers.Hash_Type (From.RGB_Part.R) or
        (Ada.Containers.Hash_Type (From.RGB_Part.G) * 16#100#) or
        (Ada.Containers.Hash_Type (From.RGB_Part.B) * 16#10000#));
end Colours;

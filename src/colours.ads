with Ada.Containers;

package Colours is
    type RGB_Component is mod 256;
    type LAB_Component is delta 2.0 ** (-8) range -128.0 .. 127.0;
    type XYZ_Component is delta 2.0 ** (-10) range 0.0 .. 1.0;
    type Angle is delta 2.0 ** (-8) range 0.0 .. 360.0;
    type Percentage is delta 2.0 ** (-8) range 0.0 .. 100.0;

    function "+" (L, R : Angle) return Angle;
    function "-" (L, R : Angle) return Angle;

    type RGB_Colour is
        record
            R, G, B : RGB_Component;
        end record;

    type LAB_Colour is
        record
            L, A, B : LAB_Component;
        end record;

    type XYZ_Colour is
        record
            X, Y, Z : XYZ_Component;
        end record;

    type HSL_Colour is
        record
            H : Angle;
            S, L : Percentage;
        end record;

    type HSV_Colour is
        record
            H : Angle;
            S, V : Percentage;
        end record;

    type CMYK_Colour is
        record
            C, M, Y, K : Percentage;
        end record;

    type Colour is private;

    -- This is the type used for all computation
    type Colour_Float is digits 18;

    type Image is array (Positive range <>, Positive range <>) of Colour;

    function Create (From : RGB_Colour) return Colour;
    function Create (From : LAB_Colour) return Colour;
    function Create (From : XYZ_Colour) return Colour;
    function Create (From : HSL_Colour) return Colour;
    function Create (From : HSV_Colour) return Colour;
    function Create (From : CMYK_Colour) return Colour;

    function RGB (From : Colour) return RGB_Colour;
    function LAB (From : Colour) return LAB_Colour;
    function XYZ (From : Colour) return XYZ_Colour;
    function HSL (From : Colour) return HSL_Colour;
    function HSV (From : Colour) return HSV_Colour;
    function CMYK (From : Colour) return CMYK_Colour;

    function RGB_Complement (From : Colour) return Colour;

    function Hash (From : Colour) return Ada.Containers.Hash_Type;
private
    type Colour is
        record
            RGB_Part : RGB_Colour;
            LAB_Part : LAB_Colour;
            XYZ_Part : XYZ_Colour;
            HSL_Part : HSL_Colour;
            HSV_Part : HSV_Colour;
            CMYK_Part : CMYK_Colour;
        end record;
end Colours;

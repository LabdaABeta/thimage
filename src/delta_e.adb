with Colours; use Colours;
with Ada.Numerics.Generic_Elementary_Functions;
with Ada.Containers.Hashed_Maps;

package body Delta_E is
    use type Ada.Containers.Hash_Type;

    package Colour_Float_Functions is
        new Ada.Numerics.Generic_Elementary_Functions (Colours.Colour_Float);
    use Colour_Float_Functions;

    type Distance_Arguments is
        record
            Left, Right : Colours.Colour;
        end record;

    function Hash (X : Distance_Arguments) return Ada.Containers.Hash_Type is (
        Hash (X.Left) xor (Hash (X.Right) * 2));

    package Memoized_Maps is new Ada.Containers.Hashed_Maps (
        Key_Type => Distance_Arguments,
        Element_Type => Colour_Float,
        Hash => Hash,
        Equivalent_Keys => "="
    );

    function ATan2_Deg (A, B : Colour_Float) return Colour_Float is
        Result : Colour_Float;
    begin
        Result := Arctan (A, B, 360.0);

        if Result < 0.0 then
            Result := Result + 360.0;
        end if;

        return Result;
    exception
        when Ada.Numerics.Argument_Error =>
            return 0.0;
    end ATan2_Deg;

    function Delta_E_76_Squared (A, B : Colours.Colour)
        return Colours.Colour_Float is
        X : constant LAB_Colour := LAB (A);
        Y : constant LAB_Colour := LAB (B);

        L1 : constant Colour_Float := Colour_Float (X.L);
        L2 : constant Colour_Float := Colour_Float (Y.L);
        A1 : constant Colour_Float := Colour_Float (X.A);
        A2 : constant Colour_Float := Colour_Float (Y.A);
        B1 : constant Colour_Float := Colour_Float (X.B);
        B2 : constant Colour_Float := Colour_Float (Y.B);
    begin
        return (L2 - L1) ** 2 + (A2 - A1) ** 2 + (B2 - B1) ** 2;
    end Delta_E_76_Squared;

    function Delta_E_94_Squared (A, B : Colours.Colour)
        return Colours.Colour_Float is
        X : constant LAB_Colour := LAB (A);
        Y : constant LAB_Colour := LAB (B);

        L1 : constant Colour_Float := Colour_Float (X.L);
        L2 : constant Colour_Float := Colour_Float (Y.L);
        A1 : constant Colour_Float := Colour_Float (X.A);
        A2 : constant Colour_Float := Colour_Float (Y.A);
        B1 : constant Colour_Float := Colour_Float (X.B);
        B2 : constant Colour_Float := Colour_Float (Y.B);

        DL : constant Colour_Float := L1 - L2;
        DA : constant Colour_Float := A1 - A2;
        DB : constant Colour_Float := B1 - B2;
        C1 : constant Colour_Float := Sqrt (A1 ** 2 + B1 ** 2);
        C2 : constant Colour_Float := Sqrt (A2 ** 2 + B2 ** 2);
        DC : constant Colour_Float := C1 - C2;
        DH : constant Colour_Float := Sqrt (DA ** 2 + DB ** 2 - DC ** 2);

        SC : constant Colour_Float := 1.0 + 0.045 * C1;
        SH : constant Colour_Float := 1.0 + 0.015 * C1;
    begin
        return (DL ** 2 + (DC / SC) ** 2 + (DH / SH) ** 2);
    end Delta_E_94_Squared;

    DE2K_Map : Memoized_Maps.Map;

    function Delta_E_2000_Squared (A, B : Colour) return Colour_Float is
        function Calculate return Colour_Float is
            L1 : constant Colour_Float := Colour_Float (LAB (A).L);
            L2 : constant Colour_Float := Colour_Float (LAB (B).L);
            A1 : constant Colour_Float := Colour_Float (LAB (A).A);
            A2 : constant Colour_Float := Colour_Float (LAB (B).A);
            B1 : constant Colour_Float := Colour_Float (LAB (A).B);
            B2 : constant Colour_Float := Colour_Float (LAB (B).B);

            C1 : constant Colour_Float := Sqrt (A1 * A1 + B1 * B1);
            C2 : constant Colour_Float := Sqrt (A2 * A2 + B2 * B2);

            C_Mean : constant Colour_Float := (C1 + C2) / 2.0;
            L_Mean : constant Colour_Float := (L1 + L2) / 2.0;

            Correction : constant Colour_Float :=
                1.0 - Sqrt (C_Mean ** 7.0 / (C_Mean ** 7 + 25.0 ** 7));

            A_1 : constant Colour_Float := A1 + A1 * Correction / 2.0;
            A_2 : constant Colour_Float := A2 + A2 * Correction / 2.0;

            C_1 : constant Colour_Float := Sqrt (A_1 * A_1 + B1 * B1);
            C_2 : constant Colour_Float := Sqrt (A_2 * A_2 + B2 * B2);

            C_Mean_Prime : constant Colour_Float := (C_1 + C_2) / 2.0;

            H_1 : constant Colour_Float := ATan2_Deg (B1, A_1);
            H_2 : constant Colour_Float := ATan2_Deg (B2, A_2);

            Delta_Hue : constant Colour_Float := (
                if abs (H_1 - H_2) <= 180.0 then
                    H_2 - H_1
                else (
                    if H_2 <= H_1 then
                        H_2 - H_1 + 360.0
                    else
                        H_2 - H_1 - 360.0));

            Delta_L : constant Colour_Float := L2 - L1;
            Delta_C : constant Colour_Float := C_2 - C_1;
            Delta_H : constant Colour_Float :=
                2.0 * Sqrt (C_1 * C_2) * Sin (Delta_Hue / 2.0, 360.0);

            H_Mean : constant Colour_Float := (
                if abs (H_1 - H_2) < 180.0 then
                    (H_1 + H_2) / 2.0
                else (
                    if H_1 + H_2 < 360.0 then
                        (H_1 + H_2 + 360.0) / 2.0
                    else
                        (H_1 + H_2 - 360.0) / 2.0));

            T : constant Colour_Float := 1.0 -
                0.17 * Cos (H_Mean - 30.0, 360.0) +
                0.24 * Cos (H_Mean * 2.0, 360.0) +
                0.32 * Cos (H_Mean * 3.0 + 6.0, 360.0) -
                0.2 * Cos (H_Mean * 4.0 - 63.0, 360.0);

            SL : constant Colour_Float := 1.0 +
                (0.015 * (L_Mean - 50.0) ** 2) /
                Sqrt (20.0 + (L_Mean - 50.0) ** 2);
            SC : constant Colour_Float := 1.0 + 0.045 * C_Mean_Prime;
            SH : constant Colour_Float := 1.0 + 0.015 * C_Mean_Prime * T;
            RT : constant Colour_Float := 2.0 *
                Sqrt ((C_Mean_Prime ** 7) / (C_Mean_Prime ** 7 + 25.0 ** 7)) *
                Sin (60.0 * Exp (-1.0 * ((H_Mean - 275.0) / 25.0) ** 2), 360.0);

            Result : constant Colour_Float :=
                (Delta_L / SL) ** 2 +
                (Delta_C / SC) ** 2 +
                (Delta_H / SH) ** 2 -
                RT * (Delta_C / SC) * (Delta_H / SH);
        begin
            if A = B then
                return 0.0;
            end if;

            return Result;
        end Calculate;

        Arguments : constant Distance_Arguments := (A, B);
        Memo_Cursor : constant Memoized_Maps.Cursor :=
            DE2K_Map.Find (Arguments);
    begin
        if Memoized_Maps.Has_Element (Memo_Cursor) then
            return Memoized_Maps.Element (Memo_Cursor);
        end if;

        declare
            Result : constant Colour_Float := Calculate;
        begin
            DE2K_Map.Insert (Arguments, Result);
            return Result;
        end;
    end Delta_E_2000_Squared;
end Delta_E;

with Colours;

package Delta_E is
    function Delta_E_76_Squared (A, B : Colours.Colour)
        return Colours.Colour_Float;
    function Delta_E_94_Squared (A, B : Colours.Colour)
        return Colours.Colour_Float;
    function Delta_E_2000_Squared (A, B : Colours.Colour)
        return Colours.Colour_Float;
end Delta_E;

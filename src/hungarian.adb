with Ada.Text_IO;
with Ada.Integer_Text_IO;
with Ada.Command_Line;

with Assignment;

procedure Hungarian is
    package Integer_Assignments is new Assignment (Integer, 0);

    Job_Count : constant Positive :=
        Positive'Value (Ada.Command_Line.Argument (1));

    Costs : Integer_Assignments.Cost_Matrix (1 .. Job_Count, 1 .. Job_Count);
    Results : Integer_Assignments.Assignments (1 .. Job_Count);
begin
    for Y in Costs'Range (1) loop
        for X in Costs'Range (2) loop
            Ada.Integer_Text_IO.Get (Costs (Y, X));
        end loop;
    end loop;

    Results := Integer_Assignments.Assign (Costs);

    for I in Results'Range loop
        Ada.Text_IO.Put_Line ("Agent" & I'Img & " does job " & Results (I)'Img);
    end loop;
end Hungarian;

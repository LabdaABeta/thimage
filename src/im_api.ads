with System;
with Interfaces.C;
with Interfaces.C.Strings;
with Interfaces.C.Pointers;

-- Thin bindings over the Image Magick C API
package IM_API is
    type Magick_Wand is new System.Address;
    type Pixel_Iterator is new System.Address;
    type Pixel_Wand is new System.Address;
    type CString is new Interfaces.C.Strings.chars_ptr;
    type CInt is new Interfaces.C.int;
    type CSize_T is new Interfaces.C.size_t;
    type CDouble is new Interfaces.C.double;

    type Pixel_Wand_Array is
        array (CSize_T range <>)
        of aliased Pixel_Wand;

    package Pixel_Wand_Pointers is new Interfaces.C.Pointers (
        Index => CSize_T,
        Element => Pixel_Wand,
        Element_Array => Pixel_Wand_Array,
        Default_Terminator => Pixel_Wand (System.Null_Address));

    procedure MagickWandGenesis;
    function NewMagickWand return Magick_Wand;
    function MagickReadImage (Wand : Magick_Wand; Image : CString) return CInt;
    procedure MagickResetIterator (Wand : Magick_Wand);
    function NewPixelIterator (Wand : Magick_Wand) return Pixel_Iterator;
    function PixelGetNextIteratorRow (
        Iterator : Pixel_Iterator;
        Number_Wands : access CSize_T)
        return Pixel_Wand_Pointers.Pointer; -- these dont need freeing
    function MagickGetImageWidth (Wand : Magick_Wand) return CSize_T;
    function MagickGetImageHeight (Wand : Magick_Wand) return CSize_T;
    function PixelGetRed (Wand : Pixel_Wand) return CDouble;
    function PixelGetGreen (Wand : Pixel_Wand) return CDouble;
    function PixelGetBlue (Wand : Pixel_Wand) return CDouble;
    procedure DestroyMagickWand (Wand : Magick_Wand);
    procedure DestroyPixelIterator (Iterator : Pixel_Iterator);
    procedure DestroyPixelWand (Wand : Pixel_Wand);
    procedure DestroyPixelWands (
        Wands : Pixel_Wand_Pointers.Pointer;
        Number_Wands : CSize_T);
    procedure MagickWandTerminus;

    pragma Import (C, MagickWandGenesis, "MagickWandGenesis");
    pragma Import (C, NewMagickWand, "NewMagickWand");
    pragma Import (C, MagickReadImage, "MagickReadImage");
    pragma Import (C, MagickResetIterator, "MagickResetIterator");
    pragma Import (C, NewPixelIterator, "NewPixelIterator");
    pragma Import (C, PixelGetNextIteratorRow, "PixelGetNextIteratorRow");
    pragma Import (C, MagickGetImageWidth, "MagickGetImageWidth");
    pragma Import (C, MagickGetImageHeight, "MagickGetImageHeight");
    pragma Import (C, PixelGetRed, "PixelGetRed");
    pragma Import (C, PixelGetGreen, "PixelGetGreen");
    pragma Import (C, PixelGetBlue, "PixelGetBlue");
    pragma Import (C, DestroyMagickWand, "DestroyMagickWand");
    pragma Import (C, DestroyPixelIterator, "DestroyPixelIterator");
    pragma Import (C, DestroyPixelWand, "DestroyPixelWand");
    pragma Import (C, DestroyPixelWands, "DestroyPixelWands");
    pragma Import (C, MagickWandTerminus, "MagickWandTerminus");
end IM_API;

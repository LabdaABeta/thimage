with Colours; use Colours;
with IM_API; use IM_API;
with Interfaces.C;

package body Image_Processing is
    procedure Initialize is
    begin
        MagickWandGenesis;
    end Initialize;

    function Load_Image (From : String) return Colours.Image is
        Wand : constant Magick_Wand := NewMagickWand;
        From_CString : CString := New_String (From);
        Status : CInt := MagickReadImage (Wand, From_CString);

        pragma Unreferenced (Status);
    begin
        MagickResetIterator (Wand);

        declare
            Width : constant CSize_T := MagickGetImageWidth (Wand);
            Height : constant CSize_T := MagickGetImageHeight (Wand);
            Iterator : constant Pixel_Iterator := NewPixelIterator (Wand);
            Count : aliased CSize_T := 0;
            The_Row : Pixel_Wand_Pointers.Pointer;

            Result : Colours.Image (
                1 .. Integer (Height),
                1 .. Integer (Width)
            );
        begin
            for Row in Result'Range (1) loop
                The_Row := PixelGetNextIteratorRow (Iterator, Count'Access);

                declare
                    Row_List : constant Pixel_Wand_Array :=
                        Pixel_Wand_Pointers.Value (
                            The_Row,
                            Interfaces.C.ptrdiff_t (Count)
                        );
                    RGB_C : RGB_Colour;
                begin
                    for Col in Result'Range (2) loop
                        RGB_C.R := RGB_Component (
                            PixelGetRed (Row_List (CSize_T (Col) - 1)) * 255.0
                        );
                        RGB_C.G := RGB_Component (
                            PixelGetGreen (Row_List (CSize_T (Col) - 1)) * 255.0
                        );
                        RGB_C.B := RGB_Component (
                            PixelGetBlue (Row_List (CSize_T (Col) - 1)) * 255.0
                        );
                        Result (Row, Col) := Create (RGB_C);
                    end loop;
                end;
            end loop;

            Free (From_CString);

            return Result;
        end;
    end Load_Image;

    procedure Finalize is
    begin
        MagickWandTerminus;
    end Finalize;
end Image_Processing;

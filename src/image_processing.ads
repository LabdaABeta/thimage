with Colours;

package Image_Processing is
    procedure Initialize;
    function Load_Image (From : String) return Colours.Image;
    procedure Finalize;
end Image_Processing;

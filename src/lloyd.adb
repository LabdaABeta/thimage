package body Lloyd is
    function K_Iterate (Clusters : Cluster_Result) return Cluster_Result is
        New_Means : C.Element_Array := Clusters.References;
    begin
        for I in New_Means'Range loop
            New_Means (I) := Mean (C.Get_Cluster (Clusters, I));
        end loop;

        return C.Cluster (Clusters.Data, New_Means);
    end K_Iterate;

    function K_Mean (Clusters : Cluster_Result) return Cluster_Result is
        Best : Cluster_Result := Clusters;
        Test : Cluster_Result := Clusters;
    begin
        loop
            Test := K_Iterate (Best);

            if Test.Score < Best.Score then
                Best := Test;
            else
                return Best;
            end if;
        end loop;
    end K_Mean;
end Lloyd;

with Clusterings;

-- Implements Lloyd's algorithm for K-Means
generic
    with package C is new Clusterings (<>);
    with function Mean (From : C.Element_Array) return C.Element;
package Lloyd is
    use C;

    -- Performs one iteration of attempted score reduction
    function K_Iterate (Clusters : Cluster_Result) return Cluster_Result;

    -- Performs K_Iterate until score stops improving
    function K_Mean (Clusters : Cluster_Result) return Cluster_Result;
end Lloyd;

with Ada.Text_IO;

package body Logging is
    procedure Log (Message : String) is
    begin
        Ada.Text_IO.Put_Line (Ada.Text_IO.Standard_Error, Message);
    end Log;

    procedure Log_Part (Message : String) is
    begin
        Ada.Text_IO.Put (Ada.Text_IO.Standard_Error, Message);
    end Log_Part;
end Logging;

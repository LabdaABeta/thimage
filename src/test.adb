with Colours;
with Lloyd;
with Clusterings;
with Colour_Printing;
with Ada.Containers;
with Image_Processing;

procedure Test is
    use type Colours.Colour_Float;

    function RGB_Manhattan (Left, Right : Colours.Colour)
        return Colours.Colour_Float
    is
        L : constant Colours.RGB_Colour := Colours.RGB (Left);
        R : constant Colours.RGB_Colour := Colours.RGB (Right);
    begin
        return
            (abs (Colours.Colour_Float (L.R) - Colours.Colour_Float (R.R))) +
            (abs (Colours.Colour_Float (L.G) - Colours.Colour_Float (R.G))) +
            (abs (Colours.Colour_Float (L.B) - Colours.Colour_Float (R.B)));
    end RGB_Manhattan;

    package Colour_Cluster is new Clusterings (
        Element => Colours.Colour,
        Length => Colours.Colour_Float,
        Zero => 0.0,
        Distance => RGB_Manhattan);

    Image_Data : Colour_Cluster.Element_Lists.List;
    Sample : Colour_Cluster.Element_Lists.List;

    function CF_Image (Item : Colours.Colour_Float) return String is
    begin
        return Colours.Colour_Float'Image (Item);
    end CF_Image;

    function Colour_Mean (
        From : Colour_Cluster.Element_Lists.List)
        return Colours.Colour
    is
        L_Sum : Colours.Colour_Float := 0.0;
        A_Sum : Colours.Colour_Float := 0.0;
        B_Sum : Colours.Colour_Float := 0.0;

        Count : Colours.Colour_Float := 0.0;
        use type Ada.Containers.Count_Type;
    begin
        if From.Length = 0 then
            return Colours.Create (Colours.RGB_Colour'(127, 127, 127));
        end if;

        for C of From loop
            L_Sum := L_Sum + Colours.Colour_Float (Colours.LAB (C).L);
            A_Sum := A_Sum + Colours.Colour_Float (Colours.LAB (C).A);
            B_Sum := B_Sum + Colours.Colour_Float (Colours.LAB (C).B);

            Count := Count + 1.0;
        end loop;

        return Colours.Create (Colours.LAB_Colour'(
            L => Colours.LAB_Component (L_Sum / Count),
            A => Colours.LAB_Component (A_Sum / Count),
            B => Colours.LAB_Component (B_Sum / Count)));
    end Colour_Mean;

    package Colour_Lloyd is new Lloyd (Colour_Cluster, Colour_Mean);

    Result : Colour_Cluster.Cluster_Result;
    Escape : constant Character := Character'Val (27);
    ANSI_Space_Format : constant String :=
        Escape & "[48;2;%1;%2;%3m " & Escape & "[0m";

    function ANSI_Block_Print (From : Colours.Colour) return String is (
        Colour_Printing.Print (ANSI_Space_Format, From));
begin
    Image_Processing.Initialize;

    declare
        Input : constant Colours.Image :=
            Image_Processing.Load_Image ("test.png");
    begin
        for Y in Input'Range (1) loop
            for X in Input'Range (2) loop
                Image_Data.Append (Input (Y, X));
            end loop;
        end loop;
    end;

    Sample.Append
        (Colours.Create (Colours.RGB_Colour'(R => 255, G => 0, B => 0)));
    Sample.Append
        (Colours.Create (Colours.RGB_Colour'(R => 0, G => 255, B => 0)));
    Sample.Append
        (Colours.Create (Colours.RGB_Colour'(R => 0, G => 0, B => 255)));
    Sample.Append
        (Colours.Create (Colours.RGB_Colour'(R => 127, G => 127, B => 127)));

    Result := Colour_Cluster.Cluster (Image_Data, Sample);

    Result := Colour_Lloyd.K_Mean (Result);
    Result := Colour_Lloyd.K_Mean (Result);

    Colour_Cluster.Dump (Result, ANSI_Block_Print'Access, CF_Image'Access);

    Image_Processing.Finalize;
end Test;

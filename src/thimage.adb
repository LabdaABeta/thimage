with Ada.Text_IO;
with Colours;
with Thimage_Stats;
with Image_Processing;
with Thimage_Arguments; use Thimage_Arguments;
with Thimage_Files; use Thimage_Files;

procedure Thimage is
    procedure Print_Theme (Name : String) is
        procedure Print_Source (Source : String) is
        begin
            Ada.Text_IO.Put_Line (" - " & Source);
        end Print_Source;
    begin
        Ada.Text_IO.Put_Line (Name & ":");
        For_Each_Source (Name, Print_Source'Access);
    end Print_Theme;

    procedure Print_Scheme (Name : String) is
        Underline : constant String (Name'Range) := (others => '~');
    begin
        Ada.Text_IO.Put_Line (Name);
        Ada.Text_IO.Put_Line (Underline);
        Ada.Text_IO.Put_Line (
            Thimage_Stats.ANSI_Image (Get_Scheme_Stats (Name)));
    end Print_Scheme;
begin
    if Parse_Arguments = Invalid_Or_Help then
        return;
    end if;

    Initialize_Theme_Directory;

    case Command is
        when Generate_Command =>
            Image_Processing.Initialize;

            declare
                Input : constant Colours.Image :=
                    Image_Processing.Load_Image (
                        if Small_Version'Length > 0 then
                            Small_Version
                        else
                            Image_File);
                Stats : constant Thimage_Stats.Image_Statistics :=
                    Thimage_Stats.Generate_Statistics (Input, 1000.0);
            begin
                Ada.Text_IO.Put_Line (Thimage_Stats.ANSI_Image (Stats));
                Put_Scheme_Stats (Scheme_Name, Stats);
                Put_Scheme_Image (Scheme_Name, Image_File);
            end;

        when List_Command =>
            case Command_Target is
                when Target_Themes =>
                    For_Each_Theme (Print_Theme'Access);

                when Target_Schemes =>
                    For_Each_Scheme (Print_Scheme'Access);
            end case;

        when Select_Command =>
            Ada.Text_IO.Put_Line (
                "TODO: Select " & Scheme_Name & " " & Theme_Name);

        when Current_Command =>
            Ada.Text_IO.Put_Line ("TODO: Current");

        when Delete_Command =>
            Ada.Text_IO.Put_Line ("TODO: Delete " & Command_Target'Img &
                Delete_Name);
    end case;
exception
    when Storage_Error =>
        Ada.Text_IO.Put_Line (
            "Out of memory, perhaps input image too large, " &
            "or stack size too small.");
end Thimage;

with Ada.Command_Line;
with Ada.Directories;
with Ada.Text_IO;
with Ada.Environment_Variables;
with GNAT.Command_Line;
with GNAT.Strings;

package body Thimage_Arguments is
    use type GNAT.Strings.String_Access;

    procedure Print_Long_Help is
        Name : constant String := Ada.Command_Line.Command_Name;
        procedure P (Line : String) is begin Ada.Text_IO.Put_Line (Line); end P;
    begin
        pragma Style_Checks (Off);
P ("");
P ("Terminology:");
P ("    colour scheme - a set of colours that, at least in theory, look good together.");
P ("    colour theme  - a set of files containing references to a colour scheme that, at least in theory, style the interface with those colours.");
P ("    It may be helpful to think of schemes as referring to colours, and themes as 'templates'.");
P ("");
P ("Generation: " & Name & " generate IMAGE-FILE [-n SCHEME-NAME] [-s SMALL-VERSION] [-d THEME-DIR]");
P ("    When used to generate colour schemes, the specified image file is processed and used to make a data file.");
P ("    This step often takes awhile, since the image has to be heavily processed.");
P ("");
P ("Listing: " & Name & " list [themes|schemes] [-d THEME-DIR]");
P ("    When used to list themes or schemes, the theme directory listing file is used to look them up.");
P ("");
P ("Selection: " & Name & " select SCHEME-NAME [THEME-NAME] [-d THEME-DIR]");
P ("    When used to select a scheme, the given theme (default is ""default"") is used to regenerate the desired files.");
P ("    The files are overwritten if they exist.");
P ("Current: " & Name & " current [-d THEME-DIR]");
P ("    Listing the current scheme lists the colours it uses as well as the files it generated.");
P ("");
P ("Delete: " & Name & " delete (theme|scheme) NAME [-d THEME-DIR]");
P ("    Deleting a theme or scheme from the directory does *not* delete the files generated from it.");
P ("    To delete those files get a listing of them using the current command.");
        pragma Style_Checks (On);
    end Print_Long_Help;

    Version : aliased Boolean;
    Usage : aliased Boolean;
    Name : aliased GNAT.Strings.String_Access;
    Small : aliased GNAT.Strings.String_Access;
    Directory : aliased GNAT.Strings.String_Access;
    Subcommand : Thimage_Subcommand := Generate_Command;
    First : GNAT.Strings.String_Access;
    Target : Target_Type := Target_Themes;

    function Parse_Arguments return Parse_Status is
        Config : GNAT.Command_Line.Command_Line_Configuration;
    begin
        GNAT.Command_Line.Define_Switch (Config, Name'Access,
            Switch => "-n:",
            Long_Switch => "--name=",
            Help => "Specify the name of the colour scheme " &
                "[default: IMAGE-FILE without its extension]",
            Argument => "NAME");

        GNAT.Command_Line.Define_Switch (Config, Small'Access,
            Switch => "-s:",
            Long_Switch => "--small=",
            Help => "Specify the name of a smaller version of the image file " &
                "for faster processing",
            Argument => "FILE");

        GNAT.Command_Line.Define_Switch (Config, Directory'Access,
            Switch => "-d:",
            Long_Switch => "--dir=",
            Help => "Specify the workspace directory for thimage to run in " &
                "[default: ~/.config/thimage/]",
            Argument => "DIRECTORY");

        GNAT.Command_Line.Define_Switch (Config, Version'Access,
            Switch => "-v",
            Long_Switch => "--version",
            Help => "Get the version of the program");

        GNAT.Command_Line.Define_Switch (Config, Usage'Access,
            Long_Switch => "--usage",
            Help => "Print a usage message");

        GNAT.Command_Line.Set_Usage (Config,
            Usage => "[generate|list|select|current|delete] [options] [args]",
            Help => "Generates themes from images.");

        begin
            GNAT.Command_Line.Getopt (Config);
        exception
            when GNAT.Command_Line.Exit_From_Command_Line =>
                Print_Long_Help;
                return Invalid_Or_Help;
        end;

        declare
            Command_Word : constant String := GNAT.Command_Line.Get_Argument;
        begin
            if Command_Word = "generate" then
                Subcommand := Generate_Command;
                First := new String'(GNAT.Command_Line.Get_Argument);

            elsif Command_Word = "list" then
                Subcommand := List_Command;
                declare
                    Next : constant String := GNAT.Command_Line.Get_Argument;
                begin
                    if Next = "themes" then
                        Target := Target_Themes;
                    elsif Next = "schemes" then
                        Target := Target_Schemes;
                    else
                        GNAT.Command_Line.Try_Help;
                        return Invalid_Or_Help;
                    end if;
                end;

            elsif Command_Word = "select" then
                Subcommand := Select_Command;
                Name := new String'(GNAT.Command_Line.Get_Argument);

                if Name.all'Length > 0 then
                    First := new String'(GNAT.Command_Line.Get_Argument);
                end if;

            elsif Command_Word = "current" then
                Subcommand := Current_Command;

            elsif Command_Word = "delete" then
                Subcommand := Delete_Command;

                declare
                    Next : constant String := GNAT.Command_Line.Get_Argument;
                begin
                    if Next = "themes" then
                        Target := Target_Themes;
                    elsif Next = "schemes" then
                        Target := Target_Schemes;
                    else
                        GNAT.Command_Line.Try_Help;
                        return Invalid_Or_Help;
                    end if;
                end;

                First := new String'(GNAT.Command_Line.Get_Argument);
            else
                GNAT.Command_Line.Try_Help;
                return Invalid_Or_Help;
            end if;
        end;

        return Valid_Arguments;
    end Parse_Arguments;

    function Command return Thimage_Subcommand is (Subcommand);

    function Theme_Directory return String is (
        if Directory /= null and then Directory.all'Length > 0 then
            Directory.all
        else Ada.Environment_Variables.Value ("HOME") & "/.config/thimage/");

    function Scheme_Name return String is (
        if Name /= null and then Name.all'Length > 0 then
            Name.all
        else Ada.Directories.Base_Name (Image_File));

    function Small_Version return String is (
        if Small /= null then Small.all else "");

    function Image_File return String is (
        if First /= null then First.all else "");

    function Command_Target return Target_Type is (Target);

    function Theme_Name return String is (
        if First /= null then First.all else "");

    function Delete_Name return String is (
        if First /= null then First.all else "");
end Thimage_Arguments;

package Thimage_Arguments is
    type Thimage_Subcommand is (
        Generate_Command,
        List_Command,
        Select_Command,
        Current_Command,
        Delete_Command
    );

    type Parse_Status is (Valid_Arguments, Invalid_Or_Help);
    function Parse_Arguments return Parse_Status;

    function Command return Thimage_Subcommand;
    function Theme_Directory return String;

    -- Select/Generation-specific
    function Scheme_Name return String;

    -- Generation-specific
    function Small_Version return String;
    function Image_File return String;

    -- List/Delete-specific
    type Target_Type is (Target_Themes, Target_Schemes);
    function Command_Target return Target_Type;

    -- Select-specific
    function Theme_Name return String;

    -- Delete-specific
    function Delete_Name return String;
end Thimage_Arguments;

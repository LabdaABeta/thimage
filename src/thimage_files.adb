with Ada.Directories;
with Ada.Sequential_IO;
with Ada.IO_Exceptions;
with Thimage_Arguments;

package body Thimage_Files is
    package Stats_IO is new Ada.Sequential_IO (Thimage_Stats.Image_Statistics);

    -- Constant names of subdirectories
    Current_Dir_Name : constant String := "current";
    Themes_Dir_Name : constant String := "themes";
    Schemes_Dir_Name : constant String := "schemes";
    Stats_File_Name : constant String := ".stats";
    Image_File_Name : constant String := "image";

    Only_Directories : constant Ada.Directories.Filter_Type := (
        Ada.Directories.Directory => True,
        others => False);
    Only_Files : constant Ada.Directories.Filter_Type := (
        Ada.Directories.Ordinary_File => True,
        others => False);

    function Full_Current_Dir_Name return String is (
        Ada.Directories.Compose (
            Containing_Directory => Thimage_Arguments.Theme_Directory,
            Name => Current_Dir_Name));

    function Full_Themes_Dir_Name return String is (
        Ada.Directories.Compose (
            Containing_Directory => Thimage_Arguments.Theme_Directory,
            Name => Themes_Dir_Name));

    function Full_Schemes_Dir_Name return String is (
        Ada.Directories.Compose (
            Containing_Directory => Thimage_Arguments.Theme_Directory,
            Name => Schemes_Dir_Name));

    function Scheme_Folder_Name (Scheme : String) return String is (
        Ada.Directories.Compose (
            Containing_Directory => Full_Schemes_Dir_Name,
            Name => Scheme));

    function Scheme_Stats_File_Name (Scheme : String) return String is (
        Ada.Directories.Compose (
            Containing_Directory => Scheme_Folder_Name (Scheme),
            Name => Stats_File_Name));

    function Theme_Folder_Name (Theme : String) return String is (
        Ada.Directories.Compose (
            Containing_Directory => Full_Themes_Dir_Name,
            Name => Theme));

    procedure Create_Dir_If_Not_Exists (Dir : String) is
    begin
        if not Ada.Directories.Exists (Dir) then
            Ada.Directories.Create_Directory (Dir);
        end if;
    end Create_Dir_If_Not_Exists;

    procedure Initialize_Theme_Directory is
    begin
        Create_Dir_If_Not_Exists (Thimage_Arguments.Theme_Directory);

        Create_Dir_If_Not_Exists (Full_Current_Dir_Name);
        Create_Dir_If_Not_Exists (Full_Themes_Dir_Name);
        Create_Dir_If_Not_Exists (Full_Schemes_Dir_Name);
    end Initialize_Theme_Directory;

    procedure For_Each_Theme (Callback : access procedure (Theme : String)) is
        procedure Search_Callback (
            Directory_Entry : Ada.Directories.Directory_Entry_Type) is
            Name : constant String :=
                Ada.Directories.Simple_Name (Directory_Entry);
        begin
            if Name (Name'First) /= '.' then
                Callback.all (Ada.Directories.Simple_Name (Directory_Entry));
            end if;
        end Search_Callback;
    begin
        Ada.Directories.Search (
            Directory => Full_Themes_Dir_Name,
            Pattern => "*",
            Filter => Only_Directories,
            Process => Search_Callback'Access);
    end For_Each_Theme;

    procedure For_Each_Scheme (Callback : access procedure (Scheme : String)) is
        procedure Search_Callback (
            Directory_Entry : Ada.Directories.Directory_Entry_Type) is
            Name : constant String :=
                Ada.Directories.Simple_Name (Directory_Entry);
        begin
            if Name (Name'First) /= '.' then
                Callback.all (Ada.Directories.Simple_Name (Directory_Entry));
            end if;
        end Search_Callback;
    begin
        Ada.Directories.Search (
            Directory => Full_Schemes_Dir_Name,
            Pattern => "*",
            Filter => Only_Directories,
            Process => Search_Callback'Access);
    end For_Each_Scheme;

    procedure Put_Scheme_Stats (
        Scheme : String;
        Stats : Thimage_Stats.Image_Statistics) is
        Stats_File : Stats_IO.File_Type;
    begin
        Create_Dir_If_Not_Exists (Scheme_Folder_Name (Scheme));

        begin
            Stats_IO.Open (
                File => Stats_File,
                Mode => Stats_IO.Out_File,
                Name => Scheme_Stats_File_Name (Scheme));
        exception
            when Ada.IO_Exceptions.Name_Error =>
                Stats_IO.Create (
                    File => Stats_File,
                    Name => Scheme_Stats_File_Name (Scheme));
        end;

        Stats_IO.Write (Stats_File, Stats);

        Stats_IO.Close (Stats_File);
    end Put_Scheme_Stats;

    procedure Put_Scheme_Image (Scheme : String; Image : String) is
        Extension : constant String := Ada.Directories.Extension (Image);

        New_Name : constant String := Ada.Directories.Compose (
            Containing_Directory => Scheme_Folder_Name (Scheme),
            Name => Image_File_Name,
            Extension => Extension);
    begin
        Create_Dir_If_Not_Exists (Scheme_Folder_Name (Scheme));

        Ada.Directories.Copy_File (Image, New_Name);
    end Put_Scheme_Image;

    function Get_Scheme_Stats (Scheme : String)
        return Thimage_Stats.Image_Statistics is
        Result : Thimage_Stats.Image_Statistics;
        Stats_File : Stats_IO.File_Type;
    begin
        Stats_IO.Open (
            File => Stats_File,
            Mode => Stats_IO.In_File,
            Name => Scheme_Stats_File_Name (Scheme));

        Stats_IO.Read (Stats_File, Result);

        Stats_IO.Close (Stats_File);

        return Result;
    end Get_Scheme_Stats;

    procedure For_Each_Source (
        Theme : String; Callback : access procedure (Source : String)) is
        procedure Search_Callback (
            Directory_Entry : Ada.Directories.Directory_Entry_Type) is
        begin
            Callback.all (Ada.Directories.Simple_Name (Directory_Entry));
        end Search_Callback;
    begin
        Ada.Directories.Search (
            Directory => Theme_Folder_Name (Theme),
            Pattern => "*",
            Filter => Only_Files,
            Process => Search_Callback'Access);
    end For_Each_Source;

    function Get_Theme_Source_Filename (Theme : String; Source : String)
        return String is (
            Ada.Directories.Compose (
                Containing_Directory => Theme_Folder_Name (Theme),
                Name => Source));
end Thimage_Files;

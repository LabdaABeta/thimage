with Thimage_Stats;

package Thimage_Files is
    Theme_Does_Not_Exist : exception;
    Scheme_Does_Not_Exist : exception;
    File_Does_Not_Exist : exception;

    procedure Initialize_Theme_Directory;

    procedure For_Each_Theme (Callback : access procedure (Theme : String));
    procedure For_Each_Scheme (Callback : access procedure (Scheme : String));

    procedure Put_Scheme_Stats (
        Scheme : String;
        Stats : Thimage_Stats.Image_Statistics);

    procedure Put_Scheme_Image (Scheme : String; Image : String);

    function Get_Scheme_Stats (Scheme : String)
        return Thimage_Stats.Image_Statistics;

    procedure For_Each_Source (
        Theme : String; Callback : access procedure (Source : String));

    function Get_Theme_Source_Filename (Theme : String; Source : String)
        return String;
end Thimage_Files;

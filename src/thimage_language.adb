package body Thimage_Language is
    Initial_Format_Str : constant String := "#%R%G%B";

    function Var_Of (From : Boolean) return Thimage_Variable is ((
        Kind => Boolean_Variable,
        Size => From'Size,
        Boolean_Value => From
    ));

    function Var_Of (From : String) return Thimage_Variable is ((
        Kind => String_Variable,
        Size => From'Length,
        String_Value => From
    ));

    function Var_Of (From : Colours.Colour) return Thimage_Variable is ((
        Kind => Colour_Variable,
        Size => From'Size,
        Colour_Value => From
    ));

    function Initialize_Language_State (From : Thimage_Stats.Image_Statistics)
        return Language_State is

        function Palette_Colour_Of (Index : Palette_Index)
            return Colours.Colour is (
                if From.Palette.Size >= Index then
                    From.Palette.Results (Index)
                else
                    Colours.Create (RGB_Colour (0, 0, 0)));

        Result : Language_State := (Symbol_Tables.Empty_Map, True);
    begin
        Result.Symbols.Include ("format", Var_Of (Initial_Format_Str));

        Result.Symbols.Include ("bright",
            Var_Of (From.Brightness = Thimage_Stats.Light));
        Result.Symbols.Include ("dim",
            Var_Of (From.Brightness = Thimage_Stats.Dark));
        Result.Symbols.Include ("warm",
            Var_Of (From.Warmth = Thimage_Stats.Warm));
        Result.Symbols.Include ("cool",
            Var_Of (From.Warmth = Thimage_Stats.Cool));

        Result.Symbols.Include ("light", Var_Of (From.Light));
        Result.Symbols.Include ("dark", Var_Of (From.Dark));

        Result.Symbols.Include ("white", Var_Of (From.Basic.White));
        Result.Symbols.Include ("black", Var_Of (From.Basic.Black));
        Result.Symbols.Include ("red", Var_Of (From.Basic.Red));
        Result.Symbols.Include ("green", Var_Of (From.Basic.Green));
        Result.Symbols.Include ("blue", Var_Of (From.Basic.Blue));
        Result.Symbols.Include ("cyan", Var_Of (From.Basic.Cyan));
        Result.Symbols.Include ("magenta", Var_Of (From.Basic.Magenta));
        Result.Symbols.Include ("yellow", Var_Of (From.Basic.Yellow));

        Result.Symbols.Include ("awhite", Var_Of (From.ANSI.White));
        Result.Symbols.Include ("ablack", Var_Of (From.ANSI.Black));
        Result.Symbols.Include ("ared", Var_Of (From.ANSI.Red));
        Result.Symbols.Include ("agreen", Var_Of (From.ANSI.Green));
        Result.Symbols.Include ("ablue", Var_Of (From.ANSI.Blue));
        Result.Symbols.Include ("acyan", Var_Of (From.ANSI.Cyan));
        Result.Symbols.Include ("amagenta", Var_Of (From.ANSI.Magenta));
        Result.Symbols.Include ("ayellow", Var_Of (From.ANSI.Yellow));
        Result.Symbols.Include ("aWhite", Var_Of (From.ANSI.Alt_White));
        Result.Symbols.Include ("aBlack", Var_Of (From.ANSI.Alt_Black));
        Result.Symbols.Include ("aRed", Var_Of (From.ANSI.Alt_Red));
        Result.Symbols.Include ("aGreen", Var_Of (From.ANSI.Alt_Green));
        Result.Symbols.Include ("aBlue", Var_Of (From.ANSI.Alt_Blue));
        Result.Symbols.Include ("aCyan", Var_Of (From.ANSI.Alt_Cyan));
        Result.Symbols.Include ("aMagenta", Var_Of (From.ANSI.Alt_Magenta));
        Result.Symbols.Include ("aYellow", Var_Of (From.ANSI.Alt_Yellow));

        Result.Symbols.Include ("primary", Var_Of (Palette_Colour_Of (1)));
        Result.Symbols.Include ("secondary", Var_Of (Palette_Colour_Of (2)));
        Result.Symbols.Include ("tertiary", Var_Of (Palette_Colour_Of (3)));
        Result.Symbols.Include ("quaternary", Var_Of (Palette_Colour_Of (4)));
        Result.Symbols.Include ("quinary", Var_Of (Palette_Colour_Of (5)));
        Result.Symbols.Include ("senary", Var_Of (Palette_Colour_Of (6)));
        Result.Symbols.Include ("septenary", Var_Of (Palette_Colour_Of (7)));
        -- TODO: Pre-initialize variables in Result.Symbols based on From
    end Initialize_Language_State;

    -- Returns the string to replace a meta sequence
    function Process_Meta_Sequence (
        State : access Language_State; -- May be modified!
        Sequence : String)
        return String;

    -- Returns False if the current state indicates not to output what follows
    function Is_Output_Included (State : Language_State) return Boolean;
end Thimage_Language;

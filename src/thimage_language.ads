with Ada.Containers.Indefinite_Hashed_Maps;
with Ada.Strings.Hash;
with Colours;
with Thimage_Stats;

package Thimage_Language is
    type Language_State is private;

    function Initialize_Language_State (From : Thimage_Stats.Image_Statistics)
        return Language_State;

    -- Returns the string to replace a meta sequence
    function Process_Meta_Sequence (
        State : access Language_State; -- May be modified!
        Sequence : String)
        return String;

    -- Returns False if the current state indicates not to output what follows
    function Is_Output_Included (State : Language_State) return Boolean;
private
    type Thimage_Variable_Type is (
        Boolean_Variable,
        Integer_Variable,
        Number_Variable,
        String_Variable,
        Colour_Variable);

    type Thimage_Variable (Kind : Thimage_Variable_Type, Size : Natural) is
        record
            case Kind is
                when Boolean_Variable =>
                    Boolean_Value : Boolean;

                when Integer_Variable =>
                    Integer_Value : Integer;

                when Number_Variable =>
                    Number_Value : Float;

                when String_Variable =>
                    String_Value : String (1 .. Size);

                when Colour_Variable =>
                    Colour_Value : Colours.Colour;
            end case;
        end record;

    package Symbol_Tables is new Indefinite_Hashed_Maps (
        Key_Type => String,
        Element_Type => Thimage_Variable,
        Hash => Ada.Strings.Hash,
        Equivalent_Keys => "=");

    type Language_State is
        record
            Symbols : Symbol_Tables.Map;
            Include : Boolean;
        end record;
end Thimage_Language;

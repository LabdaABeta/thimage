separate (Thimage_Stats)
function Calculate_Alert (
    From : Colour_Clusters.Element_Array;
    Accent : Colour;
    Against : Image_Palette
) return Colour is
    function Distance_Min (Test : Colour) return Colour_Float is
        Min : Colour_Float := Colour_Float'Last;
        Sample : Colour_Float;
    begin
        for Item of Against.Results loop
            Sample := Delta_E.Delta_E_2000_Squared (Test, Item);
            if Sample < Min then
                Min := Sample;
            end if;
        end loop;

        Sample := Delta_E.Delta_E_2000_Squared (Test, Accent);
        if Sample < Min then
            Min := Sample;
        end if;

        return Min;
    end Distance_Min;

    Candidate : Colour := From (From'First);
    Distance : Colour_Float := Distance_Min (Candidate);
    Test : Colour_Float;
begin
    for Item of From loop
        Test := Distance_Min (Item);
        if Test > Distance then
            Distance := Test;
            Candidate := Item;
        end if;
    end loop;

    return Candidate;
end Calculate_Alert;

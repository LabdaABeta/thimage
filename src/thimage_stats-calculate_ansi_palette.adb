separate (Thimage_Stats)
function Calculate_ANSI_Palette (
    From : Colour_Clusters.Element_Array;
    Accuracy : Colour_Float
) return ANSI_Palette is
    Clusters : constant Colour_Clusters.Cluster_Result :=
        Colour_Lloyd.K_Mean (
            Colour_Clusters.Cluster (From, ANSI_Palette_Samples));

    function Cluster_Colour (Index : Positive; Expected : Colour) return Colour
    is
        Reference : Colour := Clusters.References (Index);
    begin
        if Colour_Clusters.Get_Cluster_Size (Clusters, Index) = 0 then
            return Expected;
        end if;

        while Delta_E.Delta_E_2000_Squared (Reference, Expected) > Accuracy loop
            Reference := Colour_Mean ((1 => Reference, 2 => Expected));
        end loop;

        return Reference;
    end Cluster_Colour;

    Costs : Colour_Assignment.Cost_Matrix (1 .. 16, 1 .. 16);
    Results : Colour_Assignment.Assignments (1 .. 16);
begin
    Logging.Log ("ANSI Palette Clusters:");
    for Index in Clusters.References'Range loop
        Logging.Log (
            "    " &
            Colour_Printing.Print
                (ANSI_Space_Format, Clusters.References (Index)) &
            ": " & Colour_Clusters.Get_Cluster_Size (Clusters, Index)'Img);
    end loop;

    for Agent in Costs'Range (1) loop
        for Job in Costs'Range (2) loop
            Costs (Agent, Job) := Delta_E.Delta_E_2000_Squared (
                ANSI_Palette_Samples (Agent),
                Clusters.References (Job)
            );
            Logging.Log_Part (Integer (Costs (Agent, Job))'Img);
        end loop;
        Logging.Log ("");
    end loop;

    Results := Colour_Assignment.Assign (Costs);

    return (
        White => Cluster_Colour (Results (8), ANSI_Palette_Samples (8)),
        Black => Cluster_Colour (Results (1), ANSI_Palette_Samples (1)),
        Red => Cluster_Colour (Results (2), ANSI_Palette_Samples (2)),
        Green => Cluster_Colour (Results (3), ANSI_Palette_Samples (3)),
        Blue => Cluster_Colour (Results (5), ANSI_Palette_Samples (5)),
        Cyan => Cluster_Colour (Results (7), ANSI_Palette_Samples (7)),
        Magenta => Cluster_Colour (Results (6), ANSI_Palette_Samples (6)),
        Yellow => Cluster_Colour (Results (4), ANSI_Palette_Samples (4)),
        Alt_White => Cluster_Colour (Results (16), ANSI_Palette_Samples (16)),
        Alt_Black => Cluster_Colour (Results (9), ANSI_Palette_Samples (9)),
        Alt_Red => Cluster_Colour (Results (10), ANSI_Palette_Samples (10)),
        Alt_Green => Cluster_Colour (Results (11), ANSI_Palette_Samples (11)),
        Alt_Blue => Cluster_Colour (Results (13), ANSI_Palette_Samples (13)),
        Alt_Cyan => Cluster_Colour (Results (15), ANSI_Palette_Samples (15)),
        Alt_Magenta => Cluster_Colour (Results (14), ANSI_Palette_Samples (14)),
        Alt_Yellow => Cluster_Colour (Results (12), ANSI_Palette_Samples (12))
    );
end Calculate_ANSI_Palette;

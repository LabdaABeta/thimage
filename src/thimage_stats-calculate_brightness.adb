separate (Thimage_Stats)
function Calculate_Brightness (From : Colour_Clusters.Element_Array)
    return Image_Brightness
is
    Samples : constant Colour_Clusters.Element_Array :=
        Basic_Palette_Samples;
    Clusters : constant Colour_Clusters.Cluster_Result :=
        Colour_Clusters.Cluster (From, Samples);
    Biggest : Positive := 1;
    Biggest_Size : Natural := 0;
    Size : Natural;
begin
    Logging.Log ("Brightness Clusters:");

    for Index in Clusters.References'Range loop
        Logging.Log (
            "    " &
            Colour_Printing.Print
                (ANSI_Space_Format, Clusters.References (Index)) &
            ": " & Colour_Clusters.Get_Cluster_Size (Clusters, Index)'Img);
    end loop;

    for Index in Clusters.References'Range loop
        Size := Colour_Clusters.Get_Cluster_Size (Clusters, Index);

        if Size > Biggest_Size then
            Biggest := Index;
            Biggest_Size := Size;
        end if;
    end loop;

    if Biggest = 1 then
        return Dark;
    elsif Biggest = 8 then
        return Light;
    else
        return Typical;
    end if;
end Calculate_Brightness;

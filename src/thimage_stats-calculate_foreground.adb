separate (Thimage_Stats)
function Calculate_Foreground (
    Brightness : Image_Brightness;
    Against : Image_Palette
) return Colour is begin
    case Against.Size is
        when 2 =>
            if Brightness = Dark then
                return Create (RGB_Colour'(255, 255, 255));
            else
                return Create (RGB_Colour'(0, 0, 0));
            end if;

        when 3 =>
            return RGB_Complement (Against.Results (3));

        when others =>
            declare
                Furthest : Colour := Against.Results (3);
                Furthest_Distance : Colour_Float := Delta_E.Delta_E_2000_Squared
                    (Against.Results (2), Furthest);
                Test : Colour_Float;
            begin
                for I in Palette_Index range 4 .. Against.Size loop
                    Test := Delta_E.Delta_E_2000_Squared (
                        Against.Results (2), Against.Results (I));

                    if Test < Furthest_Distance then
                        Furthest := Against.Results (I);
                        Furthest_Distance := Test;
                    end if;
                end loop;

                return Furthest;
            end;
    end case;
end Calculate_Foreground;

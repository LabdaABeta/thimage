separate (Thimage_Stats)
function Calculate_Light_Dark (From : Colour_Clusters.Element_Array)
    return Colour_Pair
is
    Samples : constant Colour_Clusters.Element_Array := (
        1 => Create (RGB_Colour'(0, 0, 0)),
        2 => Create (RGB_Colour'(255, 255, 255))
    );

    Clusters : constant Colour_Clusters.Cluster_Result :=
        Colour_Lloyd.K_Mean (Colour_Clusters.Cluster (From, Samples));

    First : constant HSL_Colour := HSL (Clusters.References (1));
    Second : constant HSL_Colour := HSL (Clusters.References (2));
begin
    Logging.Log ("Light and Dark Clusters:");

    for Index in Clusters.References'Range loop
        Logging.Log (
            "    " &
            Colour_Printing.Print
                (ANSI_Space_Format, Clusters.References (Index)) &
            ": " & Colour_Clusters.Get_Cluster_Size (Clusters, Index)'Img);
    end loop;

    if First.L > Second.L then
        return (Clusters.References (1), Clusters.References (2));
    else
        return (Clusters.References (2), Clusters.References (1));
    end if;
end Calculate_Light_Dark;

separate (Thimage_Stats)
function Calculate_Palette (
    From : Colour_Clusters.Element_Array;
    Accuracy : Positive
) return Image_Palette is
    Last_Silhouette : Colour_Float := Colour_Float'First;

    function Make_Palette (From : Colour_Clusters.Cluster_Result)
        return Image_Palette is
        Result : Image_Palette (Palette_Index (From.Count));
        Referred_Clusters : array (1 .. From.Count) of Positive;
        Cluster_Size : array (1 .. From.Count) of Natural;

        procedure Sort_Clusters is
            Value : Positive;
            J : Natural;
        begin
            for I in Positive range 2 .. Referred_Clusters'Last loop
                Value := Referred_Clusters (I);
                J := I - 1;

                while J in Referred_Clusters'Range and then
                    Cluster_Size (Referred_Clusters (J)) < Cluster_Size (Value)
                loop
                    Referred_Clusters (J + 1) := Referred_Clusters (J);
                    J := J - 1;
                end loop;

                Referred_Clusters (J + 1) := Value;
            end loop;
        end Sort_Clusters;
    begin
        for I in Referred_Clusters'Range loop
            Referred_Clusters (I) := I;
            Cluster_Size (I) := Colour_Clusters.Get_Cluster_Size (From, I);
        end loop;

        Sort_Clusters;

        for I in Referred_Clusters'Range loop
            Logging.Log_Part (Referred_Clusters (I)'Img);
        end loop;
        Logging.Log ("");

        for I in Result.Results'Range loop
            Result.Results (I) := From.References (
                Referred_Clusters (Integer (I)));
        end loop;

        return Result;
    end Make_Palette;
begin
    for I in Positive range 2 .. 20 loop
        declare
            Clusters : constant Colour_Clusters.Cluster_Result :=
                Colour_Lloyd.K_Mean (
                    Colour_Clusters.Cluster (From, K_Samples (1 .. I)));
            New_Silhouette : constant Colour_Float :=
                Colour_Cluster_Silhouettes.Approximate_Silhouette (
                    Clusters, Accuracy);
        begin
            Logging.Log ("Palette of" & I'Img & " colours:");
            for Index in Clusters.References'Range loop
                Logging.Log (
                    "    " &
                    Colour_Printing.Print
                        (ANSI_Space_Format, Clusters.References (Index)) &
                    ": " &
                    Colour_Clusters.Get_Cluster_Size (Clusters, Index)'Img);
            end loop;
            Logging.Log ("Silhouette: " & New_Silhouette'Img);
            Logging.Log ("");

            if I = 20 or New_Silhouette < Last_Silhouette then
                return Make_Palette (Clusters);
            end if;

            Last_Silhouette := New_Silhouette;
        end;
    end loop;

    -- Execution should never reach here
    raise Program_Error;
end Calculate_Palette;

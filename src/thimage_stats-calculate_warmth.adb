separate (Thimage_Stats)
function Calculate_Warmth (From : Colour_Clusters.Element_Array)
    return Image_Warmth
is
    Samples : constant Colour_Clusters.Element_Array := (
        1 => Create (HSL_Colour'(0.0, 100.0, 50.0)),
        2 => Create (HSL_Colour'(135.0, 100.0, 50.0)),
        3 => Create (HSL_Colour'(270.0, 100.0, 50.0))
    );

    Clusters : constant Colour_Clusters.Cluster_Result :=
        Colour_Clusters.Cluster (From, Samples);
    Biggest : Positive := 1;
    Biggest_Size : Natural := 0;
    Size : Natural;
begin
    Logging.Log ("Warmth Clusters:");

    for Index in Clusters.References'Range loop
        Logging.Log (
            "    " &
            Colour_Printing.Print
                (ANSI_Space_Format, Clusters.References (Index)) &
            ": " & Colour_Clusters.Get_Cluster_Size (Clusters, Index)'Img);
    end loop;

    for Index in Clusters.References'Range loop
        Size := Colour_Clusters.Get_Cluster_Size (Clusters, Index);

        if Size > Biggest_Size then
            Biggest := Index;
            Biggest_Size := Size;
        end if;
    end loop;

    if Biggest = 1 then
        return Warm;
    elsif Biggest = 3 then
        return Cool;
    else
        return Typical;
    end if;
end Calculate_Warmth;

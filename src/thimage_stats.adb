with Colours; use Colours;
with Delta_E;
with Lloyd;
with Clusterings;
with Logging;
with Colour_Printing;
with Assignment;

package body Thimage_Stats is
    Escape : constant Character := Character'Val (27);
    ANSI_Space_Format : constant String :=
        Escape & "[48;2;%1;%2;%3m " & Escape & "[0m";

    type Colour_Pair is
        record
            A, B : Colours.Colour;
        end record;

    package Colour_Clusters is new Clusterings (
        Element => Colour,
        Length => Colour_Float,
        Zero => 0.0,
        Distance => Delta_E.Delta_E_2000_Squared
    );

    function Colour_Float_Mean (From : Colour_Clusters.Distance_Array)
        return Colour_Float is
        Sum : Colour_Float := 0.0;
    begin
        for X of From loop
            Sum := @ + X;
        end loop;

        if From'Length = 0 then
            return 0.0;
        else
            return Sum / Colour_Float (From'Length);
        end if;
    end Colour_Float_Mean;

    package Colour_Cluster_Silhouettes is new Colour_Clusters.Silhouettes (
        Mean => Colour_Float_Mean
    );

    function Close_Enough (A, B : Colour_Float) return Boolean is (
        abs (A - B) < 0.001
    );

    package Colour_Assignment is new Assignment (
        Cost => Colour_Float,
        Zero => 0.0,
        "+" => "+",
        "-" => "-",
        "<" => "<",
        "=" => Close_Enough
    );

    function Colour_Mean (From : Colour_Clusters.Element_Array)
        return Colour
    is
        L_Sum : Colours.Colour_Float := 0.0;
        A_Sum : Colours.Colour_Float := 0.0;
        B_Sum : Colours.Colour_Float := 0.0;

        Count : Colours.Colour_Float := 0.0;
    begin
        if From'Length = 0 then
            return Colours.Create (Colours.RGB_Colour'(127, 127, 127));
        end if;

        for C of From loop
            L_Sum := L_Sum + Colours.Colour_Float (Colours.LAB (C).L);
            A_Sum := A_Sum + Colours.Colour_Float (Colours.LAB (C).A);
            B_Sum := B_Sum + Colours.Colour_Float (Colours.LAB (C).B);

            Count := Count + 1.0;
        end loop;

        return Colours.Create (Colours.LAB_Colour'(
            L => Colours.LAB_Component (L_Sum / Count),
            A => Colours.LAB_Component (A_Sum / Count),
            B => Colours.LAB_Component (B_Sum / Count)));
    end Colour_Mean;

    package Colour_Lloyd is new Lloyd (Colour_Clusters, Colour_Mean);

    Basic_Palette_Samples : constant Colour_Clusters.Element_Array := (
        1 => Create (RGB_Colour'(0, 0, 0)),
        2 => Create (RGB_Colour'(255, 0, 0)),
        3 => Create (RGB_Colour'(0, 255, 0)),
        4 => Create (RGB_Colour'(255, 255, 0)),
        5 => Create (RGB_Colour'(0, 0, 255)),
        6 => Create (RGB_Colour'(255, 0, 255)),
        7 => Create (RGB_Colour'(0, 255, 255)),
        8 => Create (RGB_Colour'(255, 255, 255))
    );

    ANSI_Palette_Samples : constant Colour_Clusters.Element_Array := (
        1 => Create (RGB_Colour'(0, 0, 0)),
        2 => Create (RGB_Colour'(127, 0, 0)),
        3 => Create (RGB_Colour'(0, 127, 0)),
        4 => Create (RGB_Colour'(127, 127, 0)),
        5 => Create (RGB_Colour'(0, 0, 127)),
        6 => Create (RGB_Colour'(127, 0, 127)),
        7 => Create (RGB_Colour'(0, 127, 127)),
        8 => Create (RGB_Colour'(192, 192, 192)),
        9 => Create (RGB_Colour'(127, 127, 127)),
        10 => Create (RGB_Colour'(255, 0, 0)),
        11 => Create (RGB_Colour'(0, 255, 0)),
        12 => Create (RGB_Colour'(255, 255, 0)),
        13 => Create (RGB_Colour'(0, 0, 255)),
        14 => Create (RGB_Colour'(255, 0, 255)),
        15 => Create (RGB_Colour'(0, 255, 255)),
        16 => Create (RGB_Colour'(255, 255, 255))
    );

    K_Samples : constant Colour_Clusters.Element_Array := (
        1 => Create (RGB_Colour'(0, 0, 0)),
        2 => Create (RGB_Colour'(127, 0, 0)),
        3 => Create (RGB_Colour'(0, 127, 0)),
        4 => Create (RGB_Colour'(127, 127, 0)),
        5 => Create (RGB_Colour'(0, 0, 127)),
        6 => Create (RGB_Colour'(127, 0, 127)),
        7 => Create (RGB_Colour'(0, 127, 127)),
        8 => Create (RGB_Colour'(192, 192, 192)),
        9 => Create (RGB_Colour'(127, 127, 127)),
        10 => Create (RGB_Colour'(255, 0, 0)),
        11 => Create (RGB_Colour'(0, 255, 0)),
        12 => Create (RGB_Colour'(255, 255, 0)),
        13 => Create (RGB_Colour'(0, 0, 255)),
        14 => Create (RGB_Colour'(255, 0, 255)),
        15 => Create (RGB_Colour'(0, 255, 255)),
        16 => Create (RGB_Colour'(255, 255, 255)),
        17 => Create (RGB_Colour'(64, 128, 192)),
        18 => Create (RGB_Colour'(192, 128, 64)),
        19 => Create (RGB_Colour'(64, 192, 128)),
        20 => Create (RGB_Colour'(128, 192, 64))
    );

    function Calculate_Brightness (From : Colour_Clusters.Element_Array)
        return Image_Brightness
    is separate;

    function Calculate_Warmth (From : Colour_Clusters.Element_Array)
        return Image_Warmth
    is separate;

    function Calculate_Light_Dark (From : Colour_Clusters.Element_Array)
        return Colour_Pair
    is separate;

    function Calculate_Basic_Palette (
        From : Colour_Clusters.Element_Array;
        Accuracy : Colour_Float
    ) return Basic_Palette is separate;

    function Calculate_ANSI_Palette (
        From : Colour_Clusters.Element_Array;
        Accuracy : Colour_Float
    ) return ANSI_Palette is separate;

    function Calculate_Palette (
        From : Colour_Clusters.Element_Array;
        Accuracy : Positive
    ) return Image_Palette is separate;

    function Calculate_Background (
        Brightness : Image_Brightness;
        Against : Image_Palette
    ) return Colour is separate;

    function Calculate_Foreground (
        Brightness : Image_Brightness;
        Against : Image_Palette
    ) return Colour is separate;

    function Calculate_Accent (
        From : Colour_Clusters.Element_Array;
        Against : Image_Palette
    ) return Colour is separate;

    function Calculate_Alert (
        From : Colour_Clusters.Element_Array;
        Accent : Colour;
        Against : Image_Palette
    ) return Colour is separate;

    function Generate_Statistics (
        From : Colours.Image;
        Accuracy : Colour_Float
    ) return Image_Statistics is
        Data : Colour_Clusters.Element_Array
            (1 .. From'Length (1) * From'Length (2));
        Next : Positive := 1;
        Result : Image_Statistics;
        Temp : Colour_Pair;
    begin
        -- TODO: set up a concurrent processing system with exported logging
        -- (note: the logging will require a protected object)
        Logging.Log ("Processing image...");
        for Y in From'Range (1) loop
            for X in From'Range (2) loop
                Data (Next) := From (Y, X);
                Next := @ + 1;
            end loop;
        end loop;
        Logging.Log ("Loaded pixels...");

        Result.Brightness := Calculate_Brightness (Data);
        Result.Warmth := Calculate_Warmth (Data);
        Temp := Calculate_Light_Dark (Data);
        Result.Light := Temp.A;
        Result.Dark := Temp.B;
        Result.Basic := Calculate_Basic_Palette (Data, Accuracy);
        Result.ANSI := Calculate_ANSI_Palette (Data, Accuracy);
        Result.Palette := Calculate_Palette (Data, 1000);
        Result.Background :=
            Calculate_Background (Result.Brightness, Result.Palette);
        Result.Foreground :=
            Calculate_Foreground (Result.Brightness, Result.Palette);
        Result.Accent := Calculate_Accent (Data, Result.Palette);
        Result.Alert := Calculate_Alert (Data, Result.Accent, Result.Palette);

        return Result;
    end Generate_Statistics;

    ANSI_BG_Format : constant String := Escape & "[48;2;%1;%2;%3m";
    ANSI_FG_Format : constant String := Escape & "[38;2;%1;%2;%3m";
    ANSI_Reset : constant String := Escape & "[0m";

    function Colour_Text (From : Colour; Text : String) return String is (
        Colour_Printing.Print (ANSI_BG_Format, From) &
        Colour_Printing.Print (ANSI_FG_Format, RGB_Complement (From)) &
        Text & ANSI_Reset
    );

    function Palette_ANSI_Image (From : Palette_List; Index : Positive := 1)
        return String is (
            if From'Length > 0 then
                Colour_Text (From (From'First), Index'Image & " ") &
                Palette_ANSI_Image (
                    From (From'First + 1 .. From'Last), Index + 1)
            else
                ""
    );

    New_Line : constant Character := Character'Val (10);
    function ANSI_Image (From : Image_Statistics) return String is (
        "Brightness: " & From.Brightness'Img & New_Line &
        "Warmth:     " & From.Warmth'Img & New_Line &
        Colour_Text (From.Light, "LIGHT") & " " &
        Colour_Text (From.Dark, "DARK") & New_Line &
        "Basic Palette: " &
            Colour_Text (From.Basic.White, " W ") & " " &
            Colour_Text (From.Basic.Black, " K ") & " " &
            Colour_Text (From.Basic.Red, " R ") & " " &
            Colour_Text (From.Basic.Green, " G ") & " " &
            Colour_Text (From.Basic.Blue, " B ") & " " &
            Colour_Text (From.Basic.Cyan, " C ") & " " &
            Colour_Text (From.Basic.Magenta, " M ") & " " &
            Colour_Text (From.Basic.Yellow, " Y ") & New_Line &
        "ANSI Palette: " &
            Colour_Text (From.ANSI.White, " W ") & " " &
            Colour_Text (From.ANSI.Black, " K ") & " " &
            Colour_Text (From.ANSI.Red, " R ") & " " &
            Colour_Text (From.ANSI.Green, " G ") & " " &
            Colour_Text (From.ANSI.Blue, " B ") & " " &
            Colour_Text (From.ANSI.Cyan, " C ") & " " &
            Colour_Text (From.ANSI.Magenta, " M ") & " " &
            Colour_Text (From.ANSI.Yellow, " Y ") & " " &
            Colour_Text (From.ANSI.Alt_White, " w ") & " " &
            Colour_Text (From.ANSI.Alt_Black, " k ") & " " &
            Colour_Text (From.ANSI.Alt_Red, " r ") & " " &
            Colour_Text (From.ANSI.Alt_Green, " g ") & " " &
            Colour_Text (From.ANSI.Alt_Blue, " b ") & " " &
            Colour_Text (From.ANSI.Alt_Cyan, " c ") & " " &
            Colour_Text (From.ANSI.Alt_Magenta, " m ") & " " &
            Colour_Text (From.ANSI.Alt_Yellow, " y ") & New_Line &
        "Palette: " &
            Palette_ANSI_Image (From.Palette.Results) & New_Line &
        Colour_Text (From.Background, " BACKGROUND ") & " " &
        Colour_Text (From.Foreground, " FOREGROUND ") & " " &
        Colour_Printing.Print (ANSI_FG_Format, From.Foreground) &
        Colour_Printing.Print (ANSI_BG_Format, From.Background) &
        " SAMPLE " & ANSI_Reset & New_Line &
        Colour_Text (From.Accent, " ACCENT ") & " " &
        Colour_Text (From.Alert, " ALERT ") & New_Line
    );
end Thimage_Stats;

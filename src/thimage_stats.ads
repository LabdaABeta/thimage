with Colours;

package Thimage_Stats is
    type Image_Brightness is (Dark, Light, Typical);
    type Image_Warmth is (Warm, Cool, Typical);

    type Basic_Palette is
        record
            White : Colours.Colour;
            Black : Colours.Colour;
            Red : Colours.Colour;
            Green : Colours.Colour;
            Blue : Colours.Colour;
            Cyan : Colours.Colour;
            Magenta : Colours.Colour;
            Yellow : Colours.Colour;
        end record;

    type ANSI_Palette is
        record
            White : Colours.Colour;
            Black : Colours.Colour;
            Red : Colours.Colour;
            Green : Colours.Colour;
            Blue : Colours.Colour;
            Cyan : Colours.Colour;
            Magenta : Colours.Colour;
            Yellow : Colours.Colour;
            Alt_White : Colours.Colour;
            Alt_Black : Colours.Colour;
            Alt_Red : Colours.Colour;
            Alt_Green : Colours.Colour;
            Alt_Blue : Colours.Colour;
            Alt_Cyan : Colours.Colour;
            Alt_Magenta : Colours.Colour;
            Alt_Yellow : Colours.Colour;
        end record;

    type Palette_Index is range 1 .. 20;
    type Palette_List is array (Palette_Index range <>) of Colours.Colour;
    type Image_Palette (Size : Palette_Index := 20) is
        record
            Results : Palette_List (1 .. Size);
        end record;

    -- Note that while it may seem wasteful to store full Colour objects in the
    -- binary files, they still only amount to 2kb and this way no LAB/XYZ/etc
    -- information can be lost.
    type Image_Statistics is
        record
            Brightness : Image_Brightness;
            Warmth : Image_Warmth;
            Light, Dark : Colours.Colour;
            Basic : Basic_Palette;
            ANSI : ANSI_Palette;
            Palette : Image_Palette;
            Background, Foreground : Colours.Colour;
            Accent, Alert : Colours.Colour;
            Complexity, Fluidity : Float;
        end record;

    function Generate_Statistics (
        From : Colours.Image;
        Accuracy : Colours.Colour_Float
    ) return Image_Statistics;

    function ANSI_Image (From : Image_Statistics) return String;
end Thimage_Stats;

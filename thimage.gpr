project Thimage is
    for Source_Dirs use ("src");
    for Object_Dir use "obj";
    for Exec_Dir use ".";

    type Build_Type is ("Release", "Debug", "Coverage", "Profiling");
    Build_Mode : Build_Type := external ("BUILD_MODE", "Release");
    Version := external ("THIMAGE_BUILD_VERSION", "Unknown");

    for Main use ("thimage.adb", "hungarian.adb");

    Build_Compiler_Flags := ();
    Build_Linker_Flags := ();
    Build_Binder_Flags := ();

    case Build_Mode is
        when "Release" =>
            Build_Compiler_Flags := ("-O2");
        when "Debug" =>
            Build_Compiler_Flags := ("-g");
            Build_Binder_Flags := ("-E");
        when "Coverage" =>
            Build_Compiler_Flags := ("-g", "-fprofile-arcs", "-ftest-coverage");
            Build_Linker_Flags := ("-lgcov", "-fprofile-arcs");
        when "Profiling" =>
            Build_Compiler_Flags := ("-g", "-pg");
    end case;

    Style_Options := -- See gcc.gnu.org/onlinedocs/gnat_ugn/Style-Chicking.html
        "4"   & -- Proper indentation with tabs set to 4 spaces
        "a"   & -- Proper attribute casing
        "A"   & -- Clear array attributes (no index on 1D, full index on nD)
        "b"   & -- No blanks at the end of statements
        "C"   & -- Comments don't suck
        "d"   & -- No DOS EOLs
        "e"   & -- Ends/Exits from named loops must have labels
        "f"   & -- No form feeds or vtabs
        "h"   & -- No TAB characters
        "i"   & -- If then layout must be decent
        "I"   & -- No "in" parameter naming (its the default) out/in out okay.
        "k"   & -- All lowercase keywords
        "l"   & -- Proper layout
        "L12" & -- Can't nest deeper than 12 levels of nesting
        "m"   & -- Short enough lines
        "M80" & -- Maximum line length is 80 characters
        "n"   & -- Standard types are appropriately cased
        "p"   & -- Proper pragma casing
        "r"   & -- Identifiers must stay the same case
        "S"   & -- No statements on the same line as then or else
        "t"   & -- Super picky token spacing
        "u"   & -- No unnecessary blank lines
        "x"   & -- No extra parethesis (especially if (x) style)
        "";

    package Compiler is
        for Default_Switches ("Ada") use
            ("-F",                       -- Use full pathnames messages
             "-gnatef",                  -- Use full pathnames in error messages
             "-gnatU",                   -- Tag errors with "error:"
             "-gnatVa",                  -- Enable validity checking
             "-gnatwa",                  -- Enable most optional warnings
             "-gnatwe",                  -- Warnings are errors
             "-gnat2020",                -- Use Ada 202x
             "-gnaty" & Style_Options) & -- Mandate styleguide
             Build_Compiler_Flags &
             ("-gnateDVersion=""" & Version &  """");
    end Compiler;

    package Linker is
        for Default_Switches ("Ada") use Build_Linker_Flags & (
            "-lMagickWand-7.Q16HDRI",
            "-lMagickCore-7.Q16HDRI");
    end Linker;

    package Binder is
        for Default_Switches ("Ada") use Build_Binder_Flags;
    end Binder;
end Thimage;
